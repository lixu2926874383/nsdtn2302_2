# kubernetes

[toc]

## 集群图例

```mermaid
flowchart LR
subgraph C1[k8s cluster]
  M[(控制节点<br>Control Plane)] ---> N1[(node<br>计算节点)] & N2[(node<br>计算节点)] & N3[(node<br>计算节点)]
end
N1 & N2 & N3 o-.-o I((私有镜像仓库)):::IMG
User((Client)):::User -.-> M
classDef Cluster fill:#ffffc0,color:#ff00ff,stroke-width:4px
class C1 Cluster
classDef Node fill:#ccffbb,color:#000000,stroke-width:3px
class M,N1,N2,N3 Node
classDef IMG fill:#40aff0,color:#f0ff00,stroke:#f06080,stroke-width:3px
classDef User fill:#ccddee,color:#000000,stroke:#555555,stroke-width:3px;
```

## kubernetes 安装

### 仓库初始化

#### 1、创建云主机  

按照如下配置准备云主机

| 主机名    | IP地址       | 最低配置    |
| --------- | ------------ | ----------- |
| master    | 192.168.1.50 | 2CPU,4G内存 |
| node-0001 | 192.168.1.51 | 2CPU,4G内存 |
| node-0002 | 192.168.1.52 | 2CPU,4G内存 |
| node-0003 | 192.168.1.53 | 2CPU,4G内存 |
| registry  | 192.168.1.30 | 2CPU,4G内存 |

#### 2、初始化私有仓库

```shell
#配置主机名解析
[root@registry ~]# vim /etc/hosts
192.168.1.30	registry

[root@registry ~]# rm -rf /var/lib/registry/		#清理仓库
[root@registry ~]# systemctl restart docker-distribution	#重启服务
[root@registry ~]# curl  http://registry:5000/v2/_catalog	#查看结果，返回空仓库，成功
{"repositories":[]}
```

### kube-master安装

k8s官网：https://kubernetes.io/

#### 1、防火墙相关配置

参考前面知识点完成禁用 selinux，禁用 swap，卸载 firewalld-*

#### 2、配置yum仓库(跳板机)

```shell
#docker的源已经配置，此时只需要配置k8s的源即可
[root@ecs-proxy ~]# cp -r /root/5/kubernetes/packages/ /var/ftp/localrepo/
[root@ecs-proxy ~]# createrepo --update /var/ftp/localrepo/
```

#### 3、安装软件包(master)

安装kubeadm、kubectl、kubelet、docker-ce

```shell
"exec-opts":["native.cgroupdriver=systemd"] 	#设置驱动，kubelet 和 docker驱动保持一致，docker才能听懂kubelet的指令创建容器

[root@master ~]# yum install -y kubeadm kubelet kubectl docker-ce
[root@master ~]# mkdir -p /etc/docker
[root@master ~]# vim /etc/docker/daemon.json 
{
    "exec-opts":["native.cgroupdriver=systemd"],
    "registry-mirrors":["http://registry:5000"],
    "insecure-registries":["registry:5000"]
}
[root@master ~]# vim /etc/hosts
192.168.1.30	registry
192.168.1.50	master
192.168.1.51	node-0001
192.168.1.52	node-0002
192.168.1.53	node-0003
[root@master ~]# systemctl enable --now docker kubelet
[root@master ~]# docker info |grep Cgroup	#查看是否有systemd的驱动
 Cgroup Driver: systemd
 Cgroup Version: 1
```

#### 4、镜像导入私有仓库

```shell
# 拷贝v1.22.5.tar.xz 镜像到 master
[root@ecs-proxy ~]# scp /root/5/kubernetes/v1.22.5.tar.xz 192.168.1.50:/root

master主机导入镜像并上传镜像到私有仓库中
[root@master ~]# docker load -i v1.22.5.tar.xz 
[root@master ~]# vim img.sh
#!/bin/bash
docker tag k8s.gcr.io/kube-apiserver:v1.22.5 registry:5000/kube-apiserver:v1.22.5
docker push registry:5000/kube-apiserver:v1.22.5

docker tag k8s.gcr.io/kube-proxy:v1.22.5  registry:5000/kube-proxy:v1.22.5
docker push registry:5000/kube-proxy:v1.22.5

docker tag k8s.gcr.io/kube-controller-manager:v1.22.5 registry:5000/kube-controller-manager:v1.22.5
docker push registry:5000/kube-controller-manager:v1.22.5

docker tag k8s.gcr.io/kube-scheduler:v1.22.5 registry:5000/kube-scheduler:v1.22.5
docker push registry:5000/kube-scheduler:v1.22.5

docker tag k8s.gcr.io/etcd:3.5.0-0 registry:5000/etcd:3.5.0-0
docker push registry:5000/etcd:3.5.0-0

docker tag k8s.gcr.io/coredns/coredns:v1.8.4 registry:5000/coredns:v1.8.4
docker push registry:5000/coredns:v1.8.4

docker tag k8s.gcr.io/pause:3.5 registry:5000/pause:3.5
docker push registry:5000/pause:3.5

[root@master ~]# chmod +x img.sh 
[root@master ~]# ./img.sh 
[root@master ~]# curl  http://registry:5000/v2/_catalog		#查看结果，7个镜像
{"repositories":["coredns","etcd","kube-apiserver","kube-controller-manager","kube-proxy","kube-scheduler","pause"]}
```

#### 5、Tab键设置

```shell
[root@master ~]# kubectl completion bash >/etc/bash_completion.d/kubectl
[root@master ~]# kubeadm completion bash >/etc/bash_completion.d/kubeadm
[root@master ~]# exit		#退出重新登录
```

#### 6、安装代理软件包

```shell
此时配置完成之后，还不能够启动k8s集群，环境还没有配置完成，还差kube-proxy的配置，它也是k8s中的组件，作用是实现k8s service 的通信与负载均衡机制，但是它自己做不了负载均衡，它主要配置iptables和IPVS（LVS）

[root@master ~]# yum install -y ipvsadm ipset	#安装相关软件包，到时候，kube-proxy会自动配置lvs规则
```

#### 7、配置内核参数

```shell
overlay分层的文件系统，解决docker镜像分层文件系统问题 
br_netfilter网桥的防火墙，用于容器网络功能。它为 Linux 桥接设备提供了网络包过滤和修改的能力，可以帮助实现容器的网络隔离和防火墙规则的控制。

加载内核模块
[root@master ~]# for i in overlay br_netfilter
 do
 modprobe $i
 echo $i >> /etc/modules-load.d/containerd.conf
 done

启用功能
[root@master ~]# vim /etc/sysctl.d/99-kubernetes-cri.conf 
net.ipv4.ip_forward = 1
net.bridge.bridge-nf-call-iptables = 1
net.bridge.bridge-nf-call-ip6tables = 1

[root@master ~]# sysctl --system	#载入规则
```

#### 8、使用kubeadm部署

拷贝应答文件 kubernetes/config/kubeadm-init.yaml 到 master 主机

```shell
# 拷贝 kubeadm-init.yaml 到 master 云主机 init 目录下
[root@master ~]# mkdir /root/init; cd /root/init

[root@ecs-proxy ~]# scp /root/5/kubernetes/config/kubeadm-init.yaml  192.168.1.50:/root/init

[root@master init]# vim kubeadm-init.yaml 
 31 imageRepository: registry:5000		#私有镜像仓库地址
[root@master init]# kubeadm init --config=kubeadm-init.yaml  --dry-run	#--dry-run模拟安装， 输出信息若干，没有 Error 和 Warning 就是正常
 
[root@master init]# rm -rf /etc/kubernetes/tmp/		#删除检测的临时文件
[root@master init]# kubeadm init --config=kubeadm-init.yaml | tee init.log	#安装，记录日志

# 根据提示执行命令
[root@master init]#   mkdir -p $HOME/.kube
[root@master init]#   sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
[root@master init]#   sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

#### 9、验证安装结果

```shell
[root@master init]# kubectl cluster-info 		#集群运行的地址
Kubernetes control plane is running at https://192.168.1.50:6443
CoreDNS is running at https://192.168.1.50:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.

[root@master init]# kubectl get nodes		#查看节点
NAME     STATUS     ROLES                  AGE    VERSION
master   NotReady   control-plane,master   100s   v1.22.5
```

### 计算节点安装


#### 1、获取token

```shell
# 查看 token
[root@master ~]# kubeadm token list
TOKEN                     TTL         EXPIRES                
abcdef.0123456789abcdef   23h         2022-04-12T14:04:34Z
# 删除 token
[root@master ~]# kubeadm token delete abcdef.0123456789abcdef
bootstrap token "abcdef" deleted

# 创建 token，--ttl=0，设置token生命周期，0为永久；--print-join-command打印加入集群命令
[root@master ~]# kubeadm token create --ttl=0 --print-join-command
kubeadm join 192.168.1.50:6443 --token fhf6gk.bhhvsofvd672yd41 --discovery-token-ca-cert-hash sha256:ea07de5929dab8701c1bddc347155fe51c3fb6efd2ce8a4177f6dc03d5793467

# 获取token_hash
# 1、查看安装日志init.log  2、在创建token时候显示  3、使用 openssl 计算得到


读取/etc/kubernetes/pki/ca.crt文件，打印公钥；使用rsa的算法输出；转成sha256加密格式
[root@master ~]# openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt |openssl rsa -pubin -outform der |openssl dgst -sha256 -hex


使用playbook把node节点安装并加入集群
[root@ecs-proxy ~]# cd /root/5/kubernetes/nodejoin/
[root@ecs-proxy nodejoin]# vim nodeinit.yaml 
... ...
  vars:
    master: '192.168.1.50:6443'
    token: '这里改成你自己的token'
    token_hash: 'sha256:这里改成你自己的token ca hash'
... ...
[root@ecs-proxy nodejoin]# ansible-playbook nodeinit.yaml
```

#### 2、验证安装

```shell
[root@master init]# kubectl get nodes
NAME        STATUS     ROLES                  AGE     VERSION
master      NotReady   control-plane,master   8m52s   v1.22.5
node-0001   NotReady   <none>                 49s     v1.22.5
node-0002   NotReady   <none>                 49s     v1.22.5
node-0003   NotReady   <none>                 49s     v1.22.5
```

### 网络插件安装配置

#### 1、上传镜像到私有仓库

```shell
# 拷贝 kubernetes/plugins 目录到 master 云主机上
[root@ecs-proxy nodejoin]# scp -r /root/5/kubernetes/plugins/ 192.168.1.50:/root/

[root@master init]# cd /root/plugins/flannel/
[root@master flannel]# docker load -i flannel.tar.xz 

[root@master flannel]# docker tag rancher/mirrored-flannelcni-flannel:v0.16.1 registry:5000/mirrored-flannelcni-flannel:v0.16.1
[root@master flannel]# docker push registry:5000/mirrored-flannelcni-flannel:v0.16.1

[root@master flannel]# docker tag rancher/mirrored-flannelcni-flannel-cni-plugin:v1.0.0 registry:5000/mirrored-flannelcni-flannel-cni-plugin:v1.0.0
[root@master flannel]# docker push registry:5000/mirrored-flannelcni-flannel-cni-plugin:v1.0.0
```

#### 2、修改配置文件并安装

```shell
[root@master flannel]# vim kube-flannel.yml 
128       "Network": "10.244.0.0/16",
169         image: registry:5000/mirrored-flannelcni-flannel-cni-plugin:v1.0.0
180         image: registry:5000/mirrored-flannelcni-flannel:v0.16.1
194         image: registry:5000/mirrored-flannelcni-flannel:v0.16.1
[root@master flannel]# kubectl apply -f kube-flannel.yml 	#安装
```

#### 3、验证结果

```shell
# 验证节点工作状态
[root@master flannel]# kubectl get nodes
NAME           STATUS    ROLES     AGE     VERSION
master         Ready     master    26h     v1.22.5
node-0001      Ready     <none>    151m    v1.22.5
node-0002      Ready     <none>    152m    v1.22.5
node-0003      Ready     <none>    153m    v1.22.5

# 验证容器工作状态
[root@master flannel]# kubectl -n kube-system get pods
NAME                             READY   STATUS    RESTARTS   AGE
coredns-544d5fb65-g7799          1/1     Running   0          18m
coredns-544d5fb65-k9kkw          1/1     Running   0          18m
etcd-master                      1/1     Running   0          18m
kube-apiserver-master            1/1     Running   0          18m
kube-controller-manager-master   1/1     Running   0          18m
kube-flannel-ds-5fb8s            1/1     Running   0          2m6s
kube-flannel-ds-6mwpf            1/1     Running   0          2m6s
kube-flannel-ds-74mw7            1/1     Running   0          2m6s
kube-flannel-ds-fnjxb            1/1     Running   0          2m6s
kube-proxy-lrwsf                 1/1     Running   0          18m
kube-proxy-n2cgl                 1/1     Running   0          10m
kube-proxy-n6mh7                 1/1     Running   0          10m
kube-proxy-rjs5w                 1/1     Running   0          10m
kube-scheduler-master            1/1     Running   0          18m
```
