



# kubernetes

[toc]

## kubectl 管理命令

### 管理命令（一）

kubectl是用于控制k8s集群的命令行工具

| **子命令**    | **说明**                            | **备注** |
| ------------- | ----------------------------------- | -------- |
| help          | 用于查看命令及子命令的帮助信息      |          |
| cluster-info  | 显示集群的相关配置信息              |          |
| version       | 查看服务器及客户端的版本信息        |          |
| api-resources | 查看当前服务器上所有的资源对象类型  |          |
| api-versions  | 查看当前服务器上所有资源对象的版本  |          |
| config        | 管理当前节点上kubeconfig 的认证信息 |          |

#### 命令示例

```shell
# 查看帮助命令信息
[root@master ~]# kubectl help version
Print the client and server version information for the current context.

Examples:
  # Print the client and server versions for the current context
  kubectl version
... ...

# 查看集群状态信息
[root@master ~]# kubectl cluster-info 
Kubernetes control plane is running at https://192.168.1.50:6443
CoreDNS is running at https://192.168.1.50:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
... ...

# 查看服务端与客户端版本信息
[root@master ~]# kubectl version 
Client Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.5", GitCommit:"5c99e2ac2ff9a3c549d9ca665e7bc05a3e18f07e", GitTreeState:"clean", BuildDate:"2021-12-16T08:38:33Z", GoVersion:"go1.16.12", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"22", GitVersion:"v1.22.5", GitCommit:"5c99e2ac2ff9a3c549d9ca665e7bc05a3e18f07e", GitTreeState:"clean", BuildDate:"2021-12-16T08:32:32Z", GoVersion:"go1.16.12", Compiler:"gc", Platform:"linux/amd64"}

# 查看资源对象类型
[root@master ~]# kubectl api-resources 
NAME             SHORTNAMES     APIVERSION      NAMESPACED      KIND
bindings                        v1              true            Binding
endpoints        ep             v1              true            Endpoints
events           ev             v1              true            Event
... ...

# 查看资源对象版本
[root@master ~]# kubectl api-versions 
admissionregistration.k8s.io/v1
apiextensions.k8s.io/v1
apiregistration.k8s.io/v1
apps/v1
... ...

# 查看当前认证使用的用户及证书
[root@master ~]# kubectl config view 
[root@master ~]# kubectl config get-users 		#查看用户
NAME
kubernetes-admin

[root@master ~]# kubectl config get-clusters 	#查看集群
NAME
kubernetes
    
[root@master ~]# kubectl config get-contexts 	
CURRENT   NAME                          CLUSTER      AUTHINFO           NAMESPACE
*         kubernetes-admin@kubernetes   kubernetes   kubernetes-admin   
```

为registry主机配置管理授权，管理k8s集群

```shell
授权需要知道集群中各个节点的主机信息（主机名和ip），需要配置hosts文件
[root@registry ~]# vim /etc/hosts
192.168.1.30    registry
192.168.1.50    master
192.168.1.51    node-0001
192.168.1.52    node-0002
192.168.1.53    node-0003
[root@registry ~]# yum install -y kubectl

配置tab键
[root@registry ~]# kubectl completion bash > /etc/bash_completion.d/kubectl
[root@registry ~]# exit		#退出重新登录

使用证书授权，使registry主机可以管理k8s集群
[root@registry ~]# mkdir -p $HOME/.kube
[root@registry ~]# scp master:/etc/kubernetes/admin.conf $HOME/.kube/config	#授权
[root@registry ~]# sudo chown $(id -u):$(id -g) $HOME/.kube/config	#设置所有者所属组
[root@registry ~]# kubectl get nodes
NAME        STATUS   ROLES                  AGE   VERSION
master      Ready    control-plane,master   23h   v1.22.5
node-0001   Ready    <none>                 22h   v1.22.5
node-0002   Ready    <none>                 22h   v1.22.5
node-0003   Ready    <none>                 22h   v1.22.5
```

# POD详解

先准备环境创建一个pod，再来看原理

### 导入自定义镜像

```shell
# 拷贝 public/myos.tar.xz 目录到 master 云主机上
[root@ecs-proxy ~]# scp /root/5/public/myos.tar.xz 192.168.1.50:/root/

# 导入镜像，上传到私有镜像仓库
[root@master ~]# docker  load -i myos.tar.xz 
[root@master ~]# for i in nginx phpfpm v2009 httpd latest
 do
 docker tag myos:$i registry:5000/myos:$i
 docker push registry:5000/myos:$i
 docker rmi registry:5000/myos:$i
 docker rmi myos:$i
 done
 
[root@master ~]# docker tag centos:7 registry:5000/centos:7
[root@master ~]# docker push registry:5000/centos:7
[root@master ~]# docker rmi registry:5000/centos:7 
[root@master ~]# docker rmi centos:7

[root@master ~]# curl http://registry:5000/v2/myos/tags/list
{"name":"myos","tags":["phpfpm","v2009","httpd","latest","nginx"]}

[root@registry ~]# curl http://registry:5000/v2/centos/tags/list
{"name":"centos","tags":["7"]}


# 创建 Pod，名称为myweb
kubectl run pod名称 [选项/参数] --image=镜像名称:标签
[root@master ~]# kubectl run myweb --image=registry:5000/myos:httpd
pod/myweb created
[root@master ~]# kubectl get pods
NAME    READY   STATUS    RESTARTS   AGE
myweb   1/1     Running   0          26s
```



## Pod 生命周期

```mermaid
gantt
  title Pod生命周期
  dateFormat mm
  section init
    Init1   :00, 01
    Init2   :01, 02
    Init3   :02, 03
  section main
    post start     : 03, 04
    liveness probe : 04, 07
    readiness probe: 04, 07
    pre stop       : 07, 08
    main Container : crit, active, 03, 08
```



## POD创建过程

~~~mermaid
sequenceDiagram
  User ->> + API Server: create pod
  API Server ->> + etcd: write
  etcd -->> - API Server: Confirm
  API Server -->> User: Confirm
  API Server ->> + Scheduler: watch(new pod)
  Scheduler ->> - API Server: bind pod
  API Server ->> + etcd: write
  etcd -->> - API Server: Confirm
  API Server -->> Scheduler: Confirm
  API Server ->> + kubelet: watch(bound pod)
  kubelet ->> + Runtime: Runtime-shim
  Runtime -->> - kubelet: Confirm
  kubelet ->> - API Server: update pod status
  API Server ->> + etcd: write
  etcd -->> - API Server: Confirm
  API Server -->> - kubelet: Confirm
~~~

~~~shell
测试创建过程，以及调度过程
第一个终端查看，-o wide 查看更加详细的信息，-w持续检测
[root@master ~]# kubectl get pods -o wide -w

第二个终端创建pod
[root@master ~]# kubectl run web1 --image=registry:5000/myos:httpd

回到第一个终端查看结果
[root@master ~]# kubectl get pods -o wide -w
NAME    READY   STATUS             RESTARTS   AGE   IP           NODE        NOMINATED NODE   READINESS GATES
web1    0/1     Pending            0          0s    <none>       <none>      <none>           <none>
web1    0/1     Pending            0          0s    <none>       node-0003   <none>           <none>
web1    0/1     ContainerCreating   0          0s    <none>       node-0003   <none>           <none>
web1    1/1     Running             0          1s    10.244.2.4   node-0003   <none>           <none>
~~~



### 管理命令（二）

| **子命令** | **说明**               | **备注**               |
| ---------- | ---------------------- | ---------------------- |
| run        | 创建Pod资源对象        | 一般用来创建 Pod 模板  |
| get        | 查看资源对象的状态信息 | 可选参数:  -o 显示格式 |
| describe   | 查询资源对象的属性信息 |                        |
| logs       | 查看容器的报错信息     | 可选参数:  -c 容器名称 |

#### 继续创建POD

```shell
# 创建交互式 Pod
[root@master ~]# kubectl run mypod -it --image=registry:5000/myos:v2009
If you don't see a command prompt, try pressing enter.
[root@mypod /]# 			#这里已经进入Pod了

# 另外开一个终端，查询 Pod 信息
[root@master ~]# kubectl get pods
NAME    READY   STATUS    RESTARTS   AGE
mypod   1/1     Running   0          28s
myweb   1/1     Running   0          68s


[root@master ~]# kubectl get pods -o name		#-o name 只显示名字
pod/mypod
pod/myweb
[root@master ~]# kubectl get pods -o wide		#-o wide 显示更加详细的信息
NAME    READY   STATUS    RESTARTS      AGE   IP           NODE
mypod   1/1     Running   1 (39s ago)   54s   10.244.1.2   node-0001
myweb   1/1     Running   0             69m   10.244.2.2   node-0002

[root@master ~]# kubectl get pods myweb -o json		#以json格式显示
[root@master ~]# kubectl get pods myweb -o yaml		#以yaml格式显示


# 查询 myweb pod 的详细信息
[root@master ~]# kubectl describe pod myweb
Name:         myweb
... ...
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  2m11s  default-scheduler  Successfully assigned default/myweb to node-0003
  Normal  Pulling    2m10s  kubelet            Pulling image "registry:5000/myos:httpd"
  Normal  Pulled     2m4s   kubelet            Successfully pulled image "registry:5000/myos:httpd" in 6.206515294s
  Normal  Created    2m2s   kubelet            Created container myweb
  Normal  Started    2m2s   kubelet            Started container myweb

# 查询 myweb pod 的日志信息
[root@master ~]# kubectl logs myweb		#没有信息显示，也是正常的
```

查询其他名称空间的 Pod 信息

```shell
# 查询名称空间
[root@master ~]# kubectl get namespaces 
NAME              STATUS   AGE
default           Active   44m
kube-node-lease   Active   44m
kube-public       Active   44m
kube-system       Active   44m

# 查询 kube-system 名称空间下 Pod 信息
[root@master ~]# kubectl -n kube-system get pods
NAME                             READY   STATUS    RESTARTS   AGE
coredns-54b6487f4d-t7f9m         1/1     Running   0          120m
coredns-54b6487f4d-v2zbg         1/1     Running   0          120m
etcd-master                      1/1     Running   0          120m
kube-apiserver-master            1/1     Running   0          120m
kube-controller-manager-master   1/1     Running   0          120m
kube-flannel-ds-8x4hq            1/1     Running   0          111m
kube-flannel-ds-c5rkv            1/1     Running   0          111m
kube-flannel-ds-sk2gj            1/1     Running   0          111m
kube-flannel-ds-v26sx            1/1     Running   0          111m
kube-proxy-6gprw                 1/1     Running   0          115m
kube-proxy-6tfn8                 1/1     Running   0          115m
kube-proxy-9t5ln                 1/1     Running   0          115m
kube-proxy-f956k                 1/1     Running   0          120m
kube-scheduler-master            1/1     Running   0          120m
```

#### 排错练习(案例2)

```shell
注：如果在复制粘贴过程中出现格式问题，可以在末行模式下执行: set paste 调整

此案例注重排错排错排错，先不用理解含义，请先直接复制粘贴，使用排错三兄弟找出原因
[root@master ~]# vim pod1.yaml 
---
kind: Pod
apiVersion: v1
metadata:
  name: pod1
spec:
  containers:
  - name: linux
    image: registry:5000/myOs:httpd

[root@master ~]# kubectl apply -f pod1.yaml
pod/pod1 created
```

### 管理命令（三）

| **子命令** | **说明**                         | **备注**               |
| ---------- | -------------------------------- | ---------------------- |
| exec       | 在某一个容器内执行特定的命令     | 可选参数:  -c 容器名称 |
| cp         | 在容器和宿主机之间拷贝文件或目录 | 可选参数:  -c 容器名称 |
| delete     | 删除资源对象                     | 可选参数:  -f 文件名称 |
| create     | 创建资源对象                     | 必选参数:  -f 文件名称 |
| apply      | （创建/更新）资源对象            | 必选参数:  -f 文件名称 |

#### 命令示例

```shell
exec连接myweb执行命令
-- 选项终止符，后面的命令和前面的无关
[root@master ~]# kubectl exec -it myweb -- /bin/bash

拷贝myweb中的数据到本地
[root@master ~]# kubectl cp myweb:/var/www ./www

拷贝本地数据到myweb中
[root@master ~]# kubectl cp img.sh myweb:/root/
[root@master ~]# kubectl exec -it myweb -- ls /root
img.sh

delete删除资源对象，通过资源对象文件删除
[root@master ~]# kubectl delete -f pod1.yaml 

通过资源对象名字删除
[root@master ~]# kubectl delete pod mypod 

删除所有的pod，delete后面不需要加pod，因为-o name 已经查到
[root@master ~]# kubectl get pods -o name
pod/myweb
[root@master ~]# kubectl delete  $(kubectl get pods -o name)

create创建资源对象，创建名称空间abc，并删除
[root@master ~]# kubectl create namespace abc
[root@master ~]# kubectl delete namespaces abc
```

### Pod资源对象文件

#### 使用资源对象文件定义Pod

~~~yaml
五大核心字段：
---			
kind:		#资源类型，使用kubectl api-resources查看
apiVersion:	#版本，使用kubectl api-resources查看
metadata:	#元数据，该资源对象的属性信息，比如名字，标签，资源对象的注解说明等
spec:		#资源的定义，规约，定义资源的详细信息
status:		#记录资源的状态，不用配置，如创建pod是成功，失败，还是有报错等

[root@master ~]# vim myweb.yaml
---
kind: Pod
apiVersion: v1
metadata:
  name: myweb						#pod名字
spec:
  containers:						   #创建容器
  - name: apache					   #容器名字，容器可以有多个，数组表示
    image: registry:5000/myos:httpd		#使用镜像
status: {}							  #不用配置，为空，或者删掉
[root@master ~]# kubectl apply -f myweb.yaml 
[root@master ~]# kubectl get pods
NAME    READY   STATUS    RESTARTS   AGE
myweb   1/1     Running   0          2m33s

帕斯卡命名法
例如资源对象NodePort
type: NodePort
nodePort: 30010
key： 小驼峰
value：大驼峰
~~~