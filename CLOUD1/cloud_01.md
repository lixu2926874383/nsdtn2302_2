# 云计算基础 -- 虚拟化技术

[toc]

## 虚拟化平台安装


### 虚拟化实验图例

```mermaid
graph TB
  subgraph <font color=#ff0000>真机</font>
      subgraph linux
        style linux color:#ff0000,fill:#11aaff
        H1[(虚拟机)] & H2[(虚拟机)] & H3[(虚拟机)] --> B{{虚拟网桥 <font color=#ff0000>vbr</font>}} --> E([eth0])
      end
      E --> W(外部网络)
  end
```

### 验证虚拟化支持

1、虚拟化需要CPU支持（真机验证）

~~~shell
[root@server1 ~]# grep -Po "vmx|svm" /proc/cpuinfo	#/proc/cpuinfo记录了cpu的信息，以及指令集的名称；查找虚拟化的指令集
vmx
... ...

如果想要查看windows是否支持虚拟化指令集，可以双击下载好的securable程序,结果如图所示：
~~~

![image-20230604191720359](cloud_01.assets/image-20230604191720359.png)

```shell 
补充：
[root@server1 ~]# lsmod |grep kvm	#查看当前系统中正在被执行的内核模块，第一列模块命名，后面是调用以及依赖关系；modprobe 模块名 载入模块；rmmod 模块名 停止模块
kvm_intel             174841  6 
kvm                   578518  1 kvm_intel
irqbypass              13503  1 kvm

[root@server1 ~]# lsmod |grep br_netfilter	#用于网络过滤功能
[root@server1 ~]# modprobe br_netfilter		#载入模块
[root@server1 ~]# rmmod br_netfilter		#停止模块

载入模块之后，有可能需要调整内核参数：sysctl
```

2、使用真机创建虚拟机ecs，规格为 2cpu，4G内存（默认用户名: root  密码: a）  
      使用 windows 的同学，请使用 vmware 链接克隆一台新的虚拟机，按照要求更改规格

```shell
[root@server1 ~]# vm clonebase ecs
Domain ecs started                              [  OK  ]

#连接到 ecs 主机，设置主机名
[root@localhost ~]# hostnamectl set-hostname ecs
#设置ip地址
[root@localhost ~]# nmcli connection modify System\ eth0 ipv4.method manual ipv4.addresses 192.168.88.176/24 connection.autoconnect yes
[root@localhost ~]# nmcli connection up System\ eth0
[root@localhost ~]# reboot

使用vmware的需要注意想要支持虚拟化，要把如下图的对勾勾上，关机做此操作
```

![image-20230604194201199](cloud_01.assets/image-20230604194201199.png)

### 安装虚拟化平台

#### 1、安装 libvirtd 服务

```shell
[root@server1 ~]# ssh 192.168.88.176
[root@ecs ~]# vim /etc/yum.repos.d/local.repo
[BaseOS]
name=CentOS Linux $releasever - BaseOS
baseurl=ftp://192.168.88.254/centos-2009
enabled=1
gpgcheck=1

[root@ecs ~]# yum repolist
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
repo id         repo name                                   status
BaseOS          CentOS Linux 7 - BaseOS                     10,072
repolist: 10,072

#qemu-kvm仿真工具，为kvm提供底层系统设备仿真支持；
#libvirt的用户接口程序
	libvirt-daemon：是libvirtd守护进程，管理虚拟机
	libvirt-client：libvirt-client客户端软件提供客户端管理命令（virsh）
	libvirt-daemon-driver-qemu：libvirtd连接qemu的驱动

[root@ecs ~]# yum install -y qemu-kvm libvirt-daemon libvirt-daemon-driver-qemu libvirt-client		
[root@ecs ~]# systemctl enable --now libvirtd
[root@ecs ~]# virsh version
Compiled against library: libvirt 4.5.0
Using library: libvirt 4.5.0
Using API: QEMU 4.5.0
Running hypervisor: QEMU 1.5.3
```

#### 2、创建网桥 

```shell
设计规划虚拟机的网络
官方文档地址 https://libvirt.org/format.html 
网桥具体地址：https://libvirt.org/formatnetwork.html
[root@ecs ~]# vim /etc/libvirt/qemu/networks/vbr.xml
<network>
  <name>vbr</name>			#网桥的名字，关闭开启等管理网桥
  <forward mode='nat'/>		#网桥的工作模式
  <bridge name='vbr' stp='on' delay='0'/>	#这个name是ifconfig看到的网卡名，在linux中网桥（交换机）是显示不出来的，以网卡的形式存在
  <ip address='192.168.100.254' netmask='255.255.255.0'>	#网桥的IP，其实是虚拟机的网关，不同的网段通信需要做转发
    <dhcp>
      <range start='192.168.100.128' end='192.168.100.200'/>
    </dhcp>
  </ip>
</network>
[root@ecs ~]# yum install -y ebtables iptables dnsmasq	#ebtables，iptables对网络数据包进行控制，dnsmasq是DHCP和DNS服务程序，管理局域网上的IP地址和DNS查询
[root@ecs ~]# systemctl restart libvirtd
[root@ecs ~]# virsh net-define /etc/libvirt/qemu/networks/vbr.xml	#net-define根据xml文件创建虚拟网络
[root@ecs ~]# virsh net-list --all			#列出虚拟网络
[root@ecs ~]# virsh net-start vbr			#启动
[root@ecs ~]# virsh net-autostart vbr		 #开机自启
[root@ecs ~]# ifconfig 				 	    #验证
```

## Linux虚拟机

### 虚拟机磁盘ROW图例

```mermaid
flowchart LR
subgraph L0[原始盘]
  S1[(数据1)] & S2[(数据2)]
end
subgraph L1[前端盘]
  D1([数据2指针]) & D2([数据1指针]) & S3[(新数据)]
end
U1((用户)) -->|读操作| D1 ---> S2
U1((用户)) -->|写操作| D2 -->|指针重定向| S3
classDef mydisk fill:#ffffc0,color:#ff00ff
class L0,L1 mydisk
classDef X fill:#ccf,stroke:#f66,stroke-width:2px,stroke-dasharray: 10, 5
class D1,D2 X
classDef mydata fill:#0000ff,color:#ffff00,stroke:#55eecc,stroke-width:3px;
class S1,S2,S3 mydata
classDef U fill:#f0c0a0,color:#000000,stroke:#555555,stroke-width:4px;
class U1 U
```

### 创建虚拟机磁盘  

```shell
创建空的虚拟机磁盘
[root@server1 ~]# ssh 192.168.88.176
[root@ecs ~]# qemu-img create -f raw disk1.img 5G
[root@ecs ~]# qemu-img create -f qcow2 disk2.img 5G
[root@ecs ~]# ll -h  								#raw的格式是原始大小
[root@ecs ~]# rm -rf  disk1.img disk2.img

从自己linux的真机server1上面拷贝cirros.qcow2镜像到ecs主机,在ecs主机以模板创建虚拟机磁盘vmhost.img
[root@server1 ~]# scp /linux-soft/5/public/cirros.qcow2 192.168.88.176:/var/lib/libvirt/images
[root@ecs ~]# cd /var/lib/libvirt/images/
[root@ecs images]# ls
cirros.qcow2

以模板创建虚拟机磁盘，名称为vmhost.img
[root@ecs images]# qemu-img create -f qcow2 -b cirros.qcow2 vmhost.img 20G		#-b以cirros.qcow2为模板创建
[root@ecs images]# qemu-img info vmhost.img
image: vmhost.img
file format: qcow2
virtual size: 20G (21474836480 bytes)
disk size: 196K
cluster_size: 65536
backing file: cirros.qcow2
Format specific information:
    compat: 1.1
    lazy refcounts: false
```

### 虚拟机配置文件

拷贝 node_base.xml 到虚拟机中

```shell
虚拟机xml配置文件地址：https://libvirt.org/drvqemu.html#example-domain-xml-config

创建一个名称为vmhost的虚拟机
从自己linux的真机server1上面拷贝node_base.xml镜像到ecs主机,创建vmhost虚拟机的配置文件vmhost.xml
[root@server1 ~]# scp /linux-soft/5/public/node_base.xml  192.168.88.176:/root

ecs主机操作
[root@ecs images]# cp /root/node_base.xml /etc/libvirt/qemu/vmhost.xml
[root@ecs images]# cd /etc/libvirt/qemu/
[root@ecs qemu]# vim vmhost.xml
02: <name>vmhost</name>		#虚拟机类型
03: <memory unit='KB'>1024000</memory>	#最大内存，表示给虚拟机可以预留的内存
04: <currentMemory unit='KB'>1024000</currentMemory>	#当前内存，给虚拟机使用的内存
05: <vcpu placement='static'>2</vcpu>	#CPU数量
26: <source file='/var/lib/libvirt/images/vmhost.img'/>
30: <source bridge='vbr'/>
```

### 创建虚拟机

```shell
# virsh define	创建虚拟机
[root@ecs qemu]# virsh define /etc/libvirt/qemu/vmhost.xml
Domain vmhost defined from /etc/libvirt/qemu/vmhost.xml

#virsh list --all 列出所有的虚拟机，不加--all 列出启动的虚拟机
[root@ecs ~]# virsh list --all		
 Id    Name                           State
----------------------------------------------------
 -     vmhost                         shut off
 
# virsh start 启动虚拟机
[root@ecs qemu]# virsh start vmhost
域 vmhost 已开始

#virsh console 连接虚拟机
[root@ecs qemu]# virsh console vmhost		#两次回车

Connected to domain vmhost
Escape character is ^]

login as 'cirros' user. default password: 'gocubsgo'. use 'sudo' for root.
cirros login: 			#用户名：cirros，密码：gocubsgo
$ sudo -i			
# pwd
/root
# exit
$ exit		
login as 'cirros' user. default password: 'gocubsgo'. use 'sudo' for root.
cirros login:			#退出使用 ctrl + ]

```

### 常用管理命令

| 命令                        | 说明                    |
| --------------------------- | ----------------------- |
| virsh list [--all]          | 列出虚拟机              |
| virsh start/shutdown/reboot | 启动/关闭/重启虚拟机    |
| virsh destroy               | 强制停止虚拟机          |
| virsh define/undefine       | 创建/删除虚拟机         |
| virsh console               | 连接虚拟机的 console    |
| virsh edit                  | 修改虚拟机的配置        |
| virsh autostart             | 设置虚拟机自启动        |
| virsh dominfo               | 查看虚拟机摘要信息      |
| virsh domiflist             | 查看虚拟机网卡信息      |
| virsh domblklist            | 查看虚拟机硬盘信息      |
| virsh net-list [--all]      | 列出虚拟网络            |
| virsh net-start             | 启动虚拟交换机          |
| virsh net-destroy           | 强制停止虚拟交换机      |
| virsh net-define            | 根据xml文件创建虚拟网络 |
| virsh net-undefine          | 删除一个虚拟网络设备    |
| virsh net-edit              | 修改虚拟交换机的配置    |
| virsh net-autostart         | 设置开机自启动          |

~~~shell
[root@ecs qemu]# virsh list			#列出正在运行的虚拟机
 Id    名称                         状态
----------------------------------------------------
 1     vmhost                         running

[root@ecs qemu]# virsh list --all	#列出全部的虚拟机
 Id    名称                         状态
----------------------------------------------------
 1     vmhost                         running

[root@ecs qemu]# virsh console vmhost	#连接虚拟机

另外开一个终端，测试start/shutdown/reboot启动/关闭/重启虚拟机；destroy强制停止虚拟机

[root@ecs ~]# virsh reboot  vmhost
域 vmhost 正在被重新启动

[root@ecs ~]# virsh shutdown vmhost
域 vmhost 被关闭

[root@ecs ~]# virsh start  vmhost
域 vmhost 已开始

[root@ecs ~]# virsh destroy  vmhost
域 vmhost 被删除


[root@ecs ~]# virsh edit  vmhost		#修改虚拟机配置，可以使用此命令，修改完之后记得关机再开
...
  <vcpu placement='static'>1</vcpu>		#修改为1
...

[root@ecs ~]# virsh shutdown vmhost
[root@ecs ~]# virsh start  vmhost
[root@ecs qemu]# virsh  console vmhost
$ lscpu						#查看结果
Architecture:        x86_64
CPU op-mode(s):      32-bit, 64-bit
Byte Order:          Little Endian
Address sizes:       39 bits physical, 48 bits virtual
CPU(s):              1		#1个	


[root@server1 ~]# virsh  dominfo vmhost			#查看虚拟机摘要信息
[root@server1 ~]# virsh  domblklist vmhost		#查看虚拟机硬盘信息
[root@server1 ~]# virsh  domiflist vmhost		#查看虚拟机网卡信息

#删除虚拟机
[root@ecs ~]# virsh  undefine vmhost		#删除虚拟机
[root@ecs ~]# ls /etc/libvirt/qemu			#xml文件已经没有
[root@ecs ~]# ls /var/lib/libvirt/images/	#img的文件还存在，需要手动删除
cirros.qcow2  vmhost.img
[root@ecs ~]# rm -rf /var/lib/libvirt/images/vmhost.img
[root@ecs ~]# virsh  list  --all			#已经没有虚拟机

查看网络
[root@ecs ~]# virsh net-list --all
[root@ecs ~]# virsh net-destroy vbr		#销毁（停止）网络
网络 vbr 被删除

[root@ecs ~]# virsh net-list --all
 名称               状态     自动开始  持久
----------------------------------------------------------
 vbr                  不活跃  是           是

[root@ecs ~]# virsh net-edit vbr		#更改网络，执行命令即可，不需要实际修改
[root@ecs ~]# virsh net-undefine vbr	#删除一个虚拟网络设备
网络 vbr 已经被取消定义

[root@ecs ~]# ls /etc/libvirt/qemu/networks/	#已经没有vbr.xml文件
~~~

## 公有云简介

### 云服务的三大模式：

IaaS: 基础设施服务，Infrastructure-as-a-service  
PaaS: 平台服务，Platform-as-a-service  
SaaS: 软件服务，Software-as-a-service

### 公有云、私有云、混合云：

公有云是第三方的云供应商，通过互联网为广大用户提供的一种按需使用的服务器资源，是一种云基础设施。  
私有云是一个企业或组织的专用云环境。一般在企业内部使用，不对外提供服务，因此它也被视为一种企业云。  
混合云是在私有云的基础上，组合一个或多个公有云资源，从而允许在不同云环境之间共享应用和数据的使用方式。
