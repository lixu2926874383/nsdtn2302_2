# ELK日志分析平台

[toc]

## 网站云平台部署实战

### 网站架构图

```mermaid
flowchart LR
subgraph L1[<b>公有云平台</b>]
  E1([公网IP]) o--o P[(跳板机)] ----> A1[(Apache)] & A2[(Apache)] & A3[(Apache)]
  E2([公网IP]) o--o ELB((负载均衡)) -...-> A1 & A2 & A3
end
U(管理员) --->|管理| E1
Guest(最终用户) -..->|访问| E2
classDef Cloud fill:#ccffcc,color:#000000,stroke:#f66,stroke-width:3px
class L1 Cloud
classDef Cluster fill:#ccccff,color:#000066,stroke:#f66,stroke-width:3px
class P,A1,A2,A3,E1,E2 Cluster
classDef ELB fill:#ffaa44,stroke:#000000,stroke-width:3px
class ELB ELB
classDef User fill:#90f0f0,stroke:#000000,stroke-width:3px
class U User
classDef U1 fill:#8f8f8f,color:#ffffff,stroke:#020202,stroke-width:3px;
class Guest U1
```

### 系统规划

实验要求：使用做好的模板镜像，购买3台云主机，不用公网ip，部署 apache + php 的网站

| 云主机名称 | 云主机IP地址 |  云主机配置  |
| :--------: | :----------: | :----------: |
|  web-0001  | 192.168.1.11 | 2CPU，4G内存 |
|  web-0002  | 192.168.1.12 | 2CPU，4G内存 |
|  web-0003  | 192.168.1.13 | 2CPU，4G内存 |

购买时注意选择自己做的模板镜像

![image-20230606224950275](cloud_01.assets/image-20230606224950275.png)



地址直接手动分配，直接写192.168.1.11

![image-20230606225056297](cloud_01.assets/image-20230606225056297.png)

主机名直接写web，使用镜像密码

![image-20230606225238800](cloud_01.assets/image-20230606225238800.png)



### 安装部署

```shell
[root@ecs-proxy ~]# mkdir website
[root@ecs-proxy ~]# cd website
[root@ecs-proxy website]# vim ansible.cfg
[defaults]
inventory         = hostlist		#主机清单文件
host_key_checking = False			#ssh时不用输入yes
[root@ecs-proxy website]# vim hostlist
[web]
192.168.1.[11:13]
[root@ecs-proxy website]# vim install.yaml
---
- name: web 集群安装
  hosts: web
  tasks:
  - name: 安装 apache 服务 
    yum:
      name: httpd,php
      state: latest
      update_cache: yes	  				#更新远程主机上yum软件包仓库缓存：yum clean all
  - name: 配置 httpd 服务 
    service:
      name: httpd
      state: started
      enabled: yes
  - name: 部署网站网页
    unarchive:
      src: website.tar.gz
      dest: /var/www/html/
      copy: yes
      owner: apache
      group: apache

# 上传第五阶段 public/website.tar.gz 到 website 目录下
[root@ecs-proxy website]# cp /root/5/public/website.tar.gz ./
[root@ecs-proxy website]# ansible-playbook install.yaml
```

通过华为云负载均衡部署访问

![image-20230606231158267](cloud_01.assets/image-20230606231158267.png)

![image-20230606231213644](cloud_01.assets/image-20230606231213644.png)

![image-20230606231228970](cloud_01.assets/image-20230606231228970.png)

![image-20230606231308739](cloud_01.assets/image-20230606231308739.png)

![image-20230606231321555](cloud_01.assets/image-20230606231321555.png)

点击负载均衡，配置监听器

![image-20230606231347901](cloud_01.assets/image-20230606231347901.png)

![image-20230606231405559](cloud_01.assets/image-20230606231405559.png)

![image-20230606231428832](cloud_01.assets/image-20230606231428832.png)

![image-20230606231456132](cloud_01.assets/image-20230606231456132.png)

![image-20230606231523975](cloud_01.assets/image-20230606231523975.png)

![image-20230606231547223](cloud_01.assets/image-20230606231547223.png)

![image-20230606231602168](cloud_01.assets/image-20230606231602168.png)

通过浏览器查看结果，静态页面：通过负载均衡的IP进行访问

![image-20230606231855319](cloud_01.assets/image-20230606231855319.png)

php的动态页面也可以查看：负载均衡IP/info.php

## ELK架构图例

```mermaid
flowchart LR
subgraph Z1[web cluster]
    H1([apache]) o--o F1([filebeat]);H2([apache]) o--o F2([filebeat]);H3([apache]) o--o F3([filebeat])
end
subgraph Z2[Logstash]
  F1 & F2 & F3 --> A1((input)) ==> A2((filter)) ==> A3((output))
end
subgraph Z3[Elasticsearch]
  ES1(es-0001);ES2(es-0002);ES3(es-0003);ES4(es-0004);ES5(es-0005)
end
A3 --> ES1 & ES2 & ES3 & ES4 & ES5 --> K[(kibana)]
classDef APP color:#0000ff,fill:#99ff99
class H1,H2,H3,F1,F2,F3,ES1,ES2,ES3,ES4,ES5,A1,A2,A3,K APP
classDef ZONE fill:#ffffc0,color:#ff00ff
class Z1,Z2,Z3 ZONE
```

## Elasticsearch 安装

### 在跳板机上配置 yum 仓库

拷贝第五阶段elk的软件包到自定义 yum 仓库

```shell
[root@ecs-proxy ~]# cp -r 5/elk /var/ftp/localrepo/
[root@ecs-proxy ~]# createrepo --update /var/ftp/localrepo/
```

### 购买云主机 

| 主机    | IP地址       | 配置          |
| ------- | ------------ | ------------- |
| es-0001 | 192.168.1.21 | 最低配置2核4G |
| es-0002 | 192.168.1.22 | 最低配置2核4G |
| es-0003 | 192.168.1.23 | 最低配置2核4G |
| es-0004 | 192.168.1.24 | 最低配置2核4G |
| es-0005 | 192.168.1.25 | 最低配置2核4G |

### 单机安装

```shell
[root@ecs-proxy ~]# ssh 192.168.1.21
[root@es-0001 ~]# vim /etc/hosts	#配置主机名解析，让各个节点之间能够互相通讯
192.168.1.21	es-0001
192.168.1.22	es-0002
192.168.1.23	es-0003
192.168.1.24	es-0004
192.168.1.25	es-0005

[root@es-0001 ~]# yum install -y java-1.8.0-openjdk elasticsearch
[root@es-0001 ~]# vim /etc/elasticsearch/elasticsearch.yml
55:  network.host: 0.0.0.0				#监听地址，让所有主机都可以访问
[root@es-0001 ~]# systemctl enable --now elasticsearch
[root@es-0001 ~]# ss -antlp 		#监听端口9200,9300
[root@es-0001 ~]# curl http://127.0.0.1:9200/
{
  "name" : "War Eagle",
  "cluster_name" : "elasticsearch",
  "version" : {
    "number" : "2.3.4",
    "build_hash" : "e455fd0c13dceca8dbbdbb1665d068ae55dabe3f",
    "build_timestamp" : "2016-06-30T11:24:31Z",
    "build_snapshot" : false,
    "lucene_version" : "5.5.0"
  },
  "tagline" : "You Know, for Search"
}
```

### 集群安装

- 在所有主机安装 Elasticsearch


```shell
[root@es-0001 ~]# vim /etc/hosts
192.168.1.21	es-0001
192.168.1.22	es-0002
192.168.1.23	es-0003
192.168.1.24	es-0004
192.168.1.25	es-0005
[root@es-0001 ~]# yum install -y java-1.8.0-openjdk elasticsearch

[root@es-0001 ~]# vim /etc/elasticsearch/elasticsearch.yml
17:  cluster.name: my-es	#集群名称
23:  node.name: es-0001 	#本机主机名，自己是谁
55:  network.host: 0.0.0.0	#监听地址，让所有主机都可以访问
68:  discovery.zen.ping.unicast.hosts: ["es-0001", "es-0002"]	#指定集群的成员，不需要配置所有成员，三个成员即可，集群在启动时，其他节点会首先向这两个节点寻求通讯地址，所以，这里两个节点的Elasticsearch服务必须首先启动
[root@es-0001 ~]# systemctl restart elasticsearch
[root@es-0001 ~]# ss -antlp




使用playbook安装es集群
[root@ecs-proxy ~]# cd 5/public/
[root@ecs-proxy public]# tar -xf essetup.tar.gz  -C /root/
[root@ecs-proxy public]# cd /root/essetup/
[root@ecs-proxy essetup]# ansible-playbook essetup.yml 

es-0001主机验证，查看集群的状态
[root@ecs-proxy ~]# ssh 192.168.1.21
[root@es-0001 ~]# curl http://es-0001:9200/_cluster/health?pretty
{
  "cluster_name" : "my-es",
  "status" : "green",
  "timed_out" : false,
  "number_of_nodes" : 5,
  "number_of_data_nodes" : 5,
   ... ...
}
```

#### head插件安装

插件原理图

```mermaid
flowchart LR
subgraph C1[华为云]
  subgraph Host[es-0001]
    W([web服务<br>Port:80]):::Server ---|Apache<br>反向代理| S{{ES服务<br>Port:9200}}:::Server
  end
  ELB((华为云ELB<br>公网IP)) --> W
end
U1(用户) -.-> ELB
classDef Server fill:#d099ff
classDef ES color:#000000,fill:#11ccee
class Host ES
classDef U fill:#a08080,color:#ffffff,stroke:#555555,stroke-width:4px;
class U1 U
classDef ELB fill:#a0f0a0,color:#0000ff,stroke:#f66,stroke-width:2px
class EIP,ELB ELB
classDef Cluster fill:#ffffc0,color:#ff00ff,stroke-width:4px
class C1 Cluster
```

- 在 es-0001 上安装 apache，并部署 head 插件

```shell
[root@ecs-proxy ~]# scp /root/5/public/head.tar.gz 192.168.1.21:/root/	#拷贝 head.tar.gz 到 es-0001主机
[root@es-0001 ~]# yum install -y httpd
[root@es-0001 ~]# systemctl enable --now httpd
[root@es-0001 ~]# tar -xf head.tar.gz -C /var/www/html 
```

- 通过 ELB 映射 80 端口，发布 es-0001 的 web 服务到互联网

![image-20230607000055913](cloud_01.assets/image-20230607000055913.png)

![image-20230607000114152](cloud_01.assets/image-20230607000114152.png)

![image-20230607000131404](cloud_01.assets/image-20230607000131404.png)

![image-20230607000203331](cloud_01.assets/image-20230607000203331.png)

![image-20230607000228008](cloud_01.assets/image-20230607000228008.png)

![image-20230607000320666](cloud_01.assets/image-20230607000320666.png)

此时在浏览器中看到的结果为插件已经访，但是集群未连接，插件只是一个页面，想让插件管理集群，需要让插件和集群能够通过网络通信，这样才能管理集群

- es-0001 访问授权

```shell
[root@es-0001 ~]# vim /etc/httpd/conf/httpd.conf
# 配置文件最后追加
ProxyRequests off
ProxyPass /es/ http://127.0.0.1:9200/	#访问/es/时，把请求转给9200
ProxyPassReverse /es/ http://127.0.0.1:9200/
<Location ~ "^/es(-head)?/">		#认证配置
    Options None
    AuthType Basic					#认证类型
    AuthName "Elasticsearch Admin"		#认证名称
    AuthUserFile "/var/www/webauth"		#认证文件，存放用户名和密码的文件
    Require valid-user
</Location>
[root@es-0001 ~]# htpasswd -cm /var/www/webauth admin		#c：创建文件；m：使用 MD5 加密
New password: 				#密码admin
Re-type new password: 		 #密码admin
Adding password for user admin


[root@es-0001 ~]# vim /etc/elasticsearch/elasticsearch.yml	#添加授权
# 配置文件最后追加
http.cors.enabled : true
http.cors.allow-origin : "*"
http.cors.allow-methods : OPTIONS, HEAD, GET, POST, PUT, DELETE
http.cors.allow-headers : X-Requested-With,X-Auth-Token,Content-Type,Content-Length
[root@es-0001 ~]# systemctl restart elasticsearch httpd
```

使用插件访问 es 集群服务,如下图所示：

![image-20230607033024150](cloud_01.assets/image-20230607033024150.png)



## Elasticsearch基本操作

### 集群API简介

#### 集群状态查询

```shell
# 查询支持的关键字
[root@es-0001 ~]# curl -XGET http://es-0001:9200/_cat/
#查看master节点的信息
[root@es-0001 ~]# curl -XGET http://es-0001:9200/_cat/master
# 显示详细信息 ?v
[root@es-0001 ~]# curl -XGET http://es-0001:9200/_cat/master?v
# 显示帮助信息 ?help
[root@es-0001 ~]# curl -XGET http://es-0001:9200/_cat/master?help
```

#### 创建索引

图形界面创建：

![img](cloud_01.assets/clip_image002.jpg)

命令行创建：

- 指定索引的名称tedu，指定分片数量5，指定副本数量1

- 创建索引使用 PUT 方法，创建完成以后通过 head 插件验证

```shell
需要告诉es写的是一个json的数据 -H "Content-Type: application/json"
[root@es-0001 ~]# curl -XPUT -H "Content-Type: application/json" \
http://es-0001:9200/tedu -d '{
    "settings":{
       "index":{
          "number_of_shards": 5, 
          "number_of_replicas": 1
       }
    }
}'
```

#### 增加数据

在tedu的索引下，创建类型teacher，在teacher下创建数据ID号为1

```shell
[root@es-0001 ~]# curl -XPUT -H "Content-Type: application/json" \
                    http://es-0001:9200/tedu/teacher/1 -d '{
                      "职业": "讲师",
                      "名字": "欣欣",
                      "课程": "五阶段"
                  }' 
```

#### 查询数据

查询显示结果时候可以用 pretty 规范显示格式

```shell
[root@es-0001 ~]# curl -XGET http://es-0001:9200/tedu/teacher/1?pretty
```

#### 修改数据

需要指定修改数据的ID号，doc为关键字，指文档；

在doc中的值中，输入修改后的数据(以json格式书写)

```shell
[root@es-0001 ~]# curl -XPOST -H "Content-Type: application/json" \
                    http://es-0001:9200/tedu/teacher/1/_update -d '{ 
                    "doc": {"名字":"老司机"}
                  }'
```

#### 删除数据

删除时候可以是文档，也可以是索引（库），但不能是类型

```shell
# 删除一条
[root@es-0001 ~]# curl -XDELETE http://es-0001:9200/tedu/teacher/1
# 删除索引
[root@es-0001 ~]# curl -XDELETE http://es-0001:9200/tedu

浏览器查看已经删除，没有数据
```

#### 导入数据

- 日志文件在 5/public/logs.jsonl.gz

```shell
[root@ecs-proxy public]# scp /root/5/public/logs.jsonl.gz 192.168.1.21:/root/
[root@es-0001 ~]# gunzip logs.jsonl.gz 
[root@es-0001 ~]# curl -XPOST -H "Content-Type: application/json" http://es-0001:9200/_bulk --data-binary @logs.jsonl 
```

## kibana安装

### 购买云主机 

| 主机   | IP地址       | 配置          |
| ------ | ------------ | ------------- |
| kibana | 192.168.1.26 | 最低配置2核4G |

### 安装kibana

kibana 是什么：数据可视化平台工具

特点：

 \- [x] 灵活的分析和可视化平台

 \- [x] 实时总结流量和数据的图表

 \- [x] 为不同的用户显示直观的界面

 \- [x] 即时分享和嵌入的仪表板

```shell
[root@kibana ~]# vim /etc/hosts
192.168.1.21	es-0001
192.168.1.22	es-0002
192.168.1.23	es-0003
192.168.1.24	es-0004
192.168.1.25	es-0005
192.168.1.26	kibana

[root@kibana ~]# yum install -y kibana
[root@kibana ~]# vim /etc/kibana/kibana.yml
2  server.port: 5601		#监听端口
7  server.host: "0.0.0.0"	#监听地址
28  elasticsearch.hosts: ["http://es-0002:9200", "http://es-0003:9200"]		#es地址
113 i18n.locale: "zh-CN"	#中文
[root@kibana ~]# systemctl enable --now kibana
```

- 使用 ELB 发布服务，通过 WEB 浏览器访问验证

  ![image-20230607005223135](cloud_01.assets/image-20230607005223135.png)

  ![image-20230607005259904](cloud_01.assets/image-20230607005259904.png)

![image-20230607005324139](cloud_01.assets/image-20230607005324139.png)

![image-20230607005406356](cloud_01.assets/image-20230607005406356.png)

![image-20230607005449913](cloud_01.assets/image-20230607005449913.png)

![image-20230607005500367](cloud_01.assets/image-20230607005500367.png)

![image-20230607005517165](cloud_01.assets/image-20230607005517165.png)

浏览器访问查看结果：负载均衡IP:5601

![image-20230607005606908](cloud_01.assets/image-20230607005606908.png)
