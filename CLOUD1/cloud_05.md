# 容器技术

[toc]

## 基础环境

### 配置Yum仓库

把 docker 软件包添加到跳板机的自定义 yum 仓库中

```shell
[root@ecs-proxy ~]# cp -r /root/5/docker/ /var/ftp/localrepo/
[root@ecs-proxy ~]# createrepo --update /var/ftp/localrepo/
```

| 主机名      | IP地址       | 最低配置    |
| ----------- | ------------ | ----------- |
| docker-0001 | 192.168.1.31 | 2CPU,4G内存 |
| docker-0002 | 192.168.1.32 | 2CPU,4G内存 |

### docker安装

```shell
需要再docker-0001 和 docker-0002 主机操作，步骤一样，以docker-0001为例
[root@docker-0001 ~]# echo 'net.ipv4.ip_forward = 1' >>/etc/sysctl.conf
[root@docker-0001 ~]# sysctl -p
...
net.ipv4.ip_forward = 1
[root@docker-0001 ~]# yum install -y docker-ce
[root@docker-0001 ~]# systemctl enable --now docker
[root@docker-0001 ~]# docker version	#查看服务器与客户端版本
Client: Docker Engine - Community
 Version:           20.10.10
 ... ...
Server: Docker Engine - Community
 Engine:
  Version:          20.10.10
```

### 配置镜像加速

```shell
"registry-mirrors"：		#指定镜像加速站地址https://hub-mirror.c.163.com，但是此地址需要docker-0001能够上网，可以写华为的镜像加速地址，需要自己去华为云找：服务列表-》容器镜像服务SWR-》镜像资源-》镜像中心-》镜像加速器-》2. 加速器地址，把地址访问粘贴到"registry-mirrors"

添加配置文件，声明镜像加速站地址
[root@docker-0001 ~]# vim /etc/docker/daemon.json
{
    "exec-opts": ["native.cgroupdriver=systemd"],
    "registry-mirrors": ["https://08fd0a6fce0026040ffdc0158fe37d60.mirror.swr.myhuaweicloud.com"],
    "insecure-registries":[]
}
[root@docker-0001 ~]# systemctl restart docker
[root@docker-0001 ~]# docker info		#查看配置信息
... ...
 Insecure Registries:
  127.0.0.0/8
 Registry Mirrors:
  https://08fd0a6fce0026040ffdc0158fe37d60.mirror.swr.myhuaweicloud.com
 Live Restore Enabled: false
 
docker-0002也要配置
[root@docker-0001 ~]# scp /etc/docker/daemon.json 192.168.1.32:/etc/docker/
[root@docker-0002 ~]# systemctl restart docker
[root@docker-0002 ~]# docker info		
... ...
 Insecure Registries:
  127.0.0.0/8
 Registry Mirrors:
  https://08fd0a6fce0026040ffdc0158fe37d60.mirror.swr.myhuaweicloud.com
 Live Restore Enabled: false
```

# 镜像管理&容器管理

### 镜像管理命令

| 镜像管理命令                                      | 说明                                       |
| :------------------------------------------------ | :----------------------------------------- |
| docker  images                                    | 查看本机镜像                               |
| docker  search  镜像名称                          | 从官方仓库查找镜像                         |
| docker  pull  镜像名称:标签                       | 下载镜像                                   |
| docker  push  镜像名称:标签                       | 上传镜像                                   |
| docker  save  镜像名称:标签  -o  备份镜像名称.tar | 备份镜像为tar包                            |
| docker  load -i  备份镜像名称                     | 导入备份的镜像文件                         |
| docker  rmi  镜像名称:标签                        | 删除镜像（必须先删除该镜像启动的所有容器） |
| docker  history  镜像名称:标签                    | 查看镜像的制作历史                         |
| docker  inspect  镜像名称:标签                    | 查看镜像的详细信息                         |
| docker  tag  镜像名称:标签  新的镜像名称:新的标签 | 创建新的镜像名称和标签                     |

```shell
docker  images		#查看本机镜像
[root@docker-0001 ~]# docker images

docker  search  镜像名称				#从官方仓库查找镜像
[root@docker-0001 ~]# docker search busybox	 #需要联网，本次不用操作

docker  pull  镜像名称:标签				#下载镜像
[root@docker-0001 ~]# docker pull busybox	 #从华为的镜像加速站下载

docker  save  镜像名称:标签  -o  备份镜像名称.tar	#备份（导出）镜像为tar包
[root@docker-0001 ~]# docker save busybox:latest -o busybox.tar

docker  load -i  备份镜像名称		#恢复（导入）备份的镜像文件
[root@docker-0001 ~]# scp busybox.tar 192.168.1.32:/root
[root@docker-0002 ~]# docker  load -i busybox.tar


docker  rmi  镜像名称:标签		#删除镜像
[root@docker-0002 ~]# docker rmi busybox:latest 

拷贝教学环境镜像到docker-0001主机，并导入
[root@ecs-proxy ~]# scp /root/5/public/myos.tar.xz 192.168.1.31:/root/
[root@docker-0001 ~]# docker load -i myos.tar.xz

docker  history  镜像名称:标签	#查看镜像的制作历史
[root@docker-0001 ~]# docker history centos:7 

docker  inspect  镜像名称:标签	#查看镜像的详细信息
[root@docker-0001 ~]# docker inspect myos:httpd 

docker  tag  镜像名称:标签  新的镜像名称:新的标签	#创建新的镜像名称和标签
[root@docker-0001 ~]# docker tag busybox:latest nsd:test
[root@docker-0001 ~]# docker images 
```

### 容器管理命令

| 容器管理命令                                | 说明                                            |
| ------------------------------------------- | ----------------------------------------------- |
| docker  run  -it(d) 镜像名称:标签  启动命令 | 创建启动并进入一个容器                          |
| docker  ps                                  | 查看容器 -a 所有容器，包含未启动的，-q 只显示id |
| docker  rm  容器ID                          | -f 强制删除                                     |
| docker  start\|stop\|restart  容器id        | 启动、停止、重启容器                            |
| docker  exec  -it  容器id  启动命令         | 在容器内执行命令                                |
| docker  cp  本机文件路径  容器id:容器内路径 | 把本机文件拷贝到容器内（上传）                  |
| docker  cp  容器id:容器内路径  本机文件路径 | 把容器内文件拷贝到本机（下载）                  |
| docker  inspect  容器ID                     | 查看容器的详细信息                              |
| docker  logs  容器ID                        | 查看容器日志                                    |

```shell
启动容器
docker  run 命令：docker   run   -参数   镜像名称:镜像标签    启动命令
docker-0001根据myos:v2009镜像启动并进入一个容器
[root@docker-0001 ~]# docker  run  -it  myos:v2009  /bin/bash		#/bin/bash为容器内的命令,容器内存在，才可以使用

[root@89d1ec3a0dc8 /]# exit
[root@docker-0001 ~]# docker  run  -itd myos:httpd  	#启动httpd，是个服务，要加d，放后台才能运行

启动容器时指定名字
[root@docker-0001 ~]# docker run -itd --name apache myos:httpd 

查看容器：docker   ps   [ -a   所有容器id ]   [  -q  只显示容器 id  ]
[root@docker-0001 ~]# docker ps
[root@docker-0001 ~]# docker ps -a		#ps  -a 查看创建的所有的容器(运行的和已经停止的)
[root@docker-0001 ~]# docker ps -q		#ps -q 查看运行中容器的ID值
[root@docker-0001 ~]# docker ps -aq		#ps -aq查看所有容器的id，用于脚本对容器的管理

删除容器：docker   rm   容器id
[root@docker-0001 ~]# docker rm 512567e6f64f

删除正在使用的容器时，会报错，无法删除，需要先停止容器，再执行删除命令
[root@docker-0001 ~]# docker stop 512567e6f64f		#停止容器
[root@docker-0001 ~]# docker rm 512567e6f64f

删除所有的容器：支持命令行重录，前一个命令的结果可以作为后一个命令的参数
[root@docker-0001 ~]# docker rm -f $(docker ps -aq)		#$()用来获取命令的执行结果

临时启动容器 --rm，容器结束后自动删除，在内存中运行，退出后直接没有
第一个终端启动centos:7容器
[root@docker-0001 ~]# docker run -it --name test1 centos:7 
[root@e5f780c059f6 /]# 

第二个终端查看，可以看到结果
[root@docker-0001 ~]# docker ps | grep test1

回到第一个终端，退出容器
[root@e5f780c059f6 /]# exit

在第二个终端查看，可以看到已经停止，但是容器还在，没有删除
[root@docker-0001 ~]# docker ps  -a | grep test1


此时在第一个终端再次创建一个容器test2，加上--rm的选项查看结果
[root@docker-0001 ~]# docker run -it --name test2 --rm centos:7

第二个终端查看，可以看到结果
[root@docker-0001 ~]# docker ps | grep test2

回到第一个终端，退出容器
[root@e5f780c059f6 /]# exit

在第二个终端查看，可以看到已经停止，但是容器已经没有
[root@docker-0001 ~]# docker ps  -a | grep test2

容器管理命令启动、停止、重启
  - [x] docker   start   	容器id
  - [x] docker   stop   	容器id
  - [x] docker   restart   	容器id

[root@docker-0001 ~]# docker rm -f $(docker ps -aq)
[root@docker-0001 ~]# docker run -itd myos:nginx 
查看容器信息：docker   inspect    容器id
[root@docker-0001 ~]# docker inspect 38b8ee2a8754
......
                    "Gateway": "172.17.0.1",
                    "IPAddress": "172.17.0.2",
......
[root@docker-0001 ~]# curl http://172.17.0.2
Nginx is running !

[root@docker-0001 ~]# docker stop 38b8ee2a8754
[root@docker-0001 ~]# docker start 38b8ee2a8754
[root@docker-0001 ~]# docker restart 38b8ee2a8754

拷贝文件
docker cp 本机文件路径      容器id:容器内路径（上传）
docker cp 容器id:容器内路径  本机文件路径（下载）

拷贝宿主机文件到容器
#在刚刚启动的第一个容器终端中执行
[root@docker-0001 ~]# docker run -it --name test myos:v2009 

#另外开一个docker-0001的终端操作
[root@docker-0001 ~]# docker cp  /root/busybox.tar test:/root/	

在刚刚启动的第一个容器终端中查看结果
[root@b6a1ab762bd5 /]# ls /root/
busybox.tar


拷贝容器内文件到宿主机
[root@docker-0001 ~]# docker cp test:/etc/hosts ./
[root@docker-0001 ~]# ls		#有hosts文件
busybox.tar  hosts  myos.tar.xz

# 查看容器日志  docker logs 容器ID
[root@docker-0001 ~]# docker run  -itd myos:nginx 
[root@docker-0001 ~]# docker logs  99d		#99d为nginx容器的id，logs查看没有信息

[root@docker-0001 ~]# docker inspect 99d
[root@docker-0001 ~]# curl 172.17.0.3/wfr		#访问一个不存在的页面
<html>
<head><title>404 Not Found</title></head>
<body>
<center><h1>404 Not Found</h1></center>
<hr><center>nginx/1.17.6</center>
</body>
</html>
[root@docker-0001 ~]# docker logs  99d		#再次查看有，有信息
2023/06/11 12:45:32 [error] 7#0: *1 open() "/usr/local/nginx/html/wfr" failed (2: No such file or directory), client: 172.17.0.1, server: localhost, request: "GET /wfr HTTP/1.1", host: "172.17.0.3"

但是如果apache的服务也是同样测试，可能结果就不显示，是因为软件原因


连接容器，启动新进程：docker   exec   -it    容器id    命令
[root@docker-0001 ~]# docker run  -it myos:v2009
[root@164a7b1a6c57 /]# exit		#此时容器时关闭的

[root@docker-0001 ~]# docker start  164a7b1a6c57	#启动myos:v2009的容器
[root@docker-0001 ~]# docker exec -it 164a7b1a6c57 /bin/bash		#根据容器ID，进入容器
[root@164a7b1a6c57 /]# ps -ef			#两个bash进程
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 12:49 pts/0    00:00:00 /bin/bash
root        42     0  0 12:50 pts/1    00:00:00 /bin/bash
root        63    42  0 12:50 pts/1    00:00:00 ps -ef		
[root@164a7b1a6c57 /]# exit		     #退出

连接容器启动进程:docker    attach   容器id
attach 以上帝进程的身份，进入容器内部，当执行exit退出容器时，会结束整个容器，通常用于在测试时，查看报错信息；
[root@docker-0001 ~]# docker attach 164a7b1a6c57
[root@164a7b1a6c57 /]# ps -ef				#一个bash进程（上帝进程）
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 12:49 pts/0    00:00:00 /bin/bash
root        64     1  0 12:52 pts/0    00:00:00 ps -ef
[root@164a7b1a6c57 /]# exit			#退出，容器关闭


[root@docker-0001 ~]# docker start 164a7b1a6c57		#启动被关闭容器
[root@docker-0001 ~]# docker attach 164a7b1a6c57	#Ctrl + p + q  退出容器，但不会停止容器的运行
		
#在对容器的使用过程中，都是使用exec，新开一个进程的方式进入容器，进行操作；
#而attach 往往应用于容器内部进行交互、排查问题、调试等操作；
```

### 简单镜像制作

```shell
获取基础镜像
方法一：docker-0001主机备份centos:7镜像，拷贝到docker-0002导入
[root@docker-0001 ~]# docker save centos:7 -o centos.tar
[root@docker-0001 ~]# scp centos.tar 192.168.1.32:/root/
[root@docker-0002 ~]# docker load -i centos.tar 

方法二：docker-0002主机直接下载
[root@docker-0002 ~]# docker pull centos:7

commit 自定义镜像
使用镜像启动容器，在该容器基础上修改，另存为一个新镜像
  - [x] docker  run  -it    centos:latest   /bin/bash
  - [x] 配置 yum，安装软件，系统配置
  - [x] docker  commit  容器id   新镜像名称: 新镜像标签
  
在docker-0001创建一个centos的容器
[root@docker-0002 ~]# docker run -it --name mycentos centos:7
[root@cb458c4e9ede /]# cd /etc/yum.repos.d/       
[root@cb458c4e9ede yum.repos.d]# rm -rf *
[root@cb458c4e9ede yum.repos.d]# curl http://mirrors.myhuaweicloud.com/repo/CentOS-Base-7.repo -so /etc/yum.repos.d/CentOS-Base.repo	#下载华为云的yum仓库
[root@cb458c4e9ede yum.repos.d]# yum -y install net-tools psmisc iproute vim-enhanced bash-completion tree
[root@cb458c4e9ede yum.repos.d]# yum clean all		#清除yum缓存，减小容器大小，用于镜像制作
[root@cb458c4e9ede yum.repos.d]# exit 

commit 提交容器，生成新的镜像；mycentos为容器的名字
[root@docker-0002 ~]# docker commit mycentos mycentos:latest
[root@docker-0002 ~]# docker images		#mycentos就是新生成的镜像
[root@docker-0002 ~]# docker rm mycentos	#删除容器

使用 docker  run 验证新的镜像
[root@docker-0002 ~]# docker history mycentos:latest		#查看历史镜像，多出一个镜像层
[root@docker-0002 ~]# docker run -it mycentos:latest 
[root@aa14feea7b4e /]# yum repolist		
[root@aa14feea7b4e /]# ifconfig
[root@aa14feea7b4e /]# exit
```

### 容器内部署应用

```shell
删除之前所有的容器
[root@docker-0002 ~]# docker rm -f $(docker ps -aq)
[root@docker-0002 ~]# docker run -it --name myapache mycentos:latest

此时就可以在容器里面安装相关的软件包
[root@e0e8f8596277 /]# yum -y install httpd php
[root@e0e8f8596277 /]# echo 'hello world' > /var/www/html/index.html
[root@e0e8f8596277 /]# pstree -p		#只有bash的进程

#因为容器内并没有systemd的服务，无法使用systemctl来启动httpd的服务
#查看httpd的服务文件，获取环境变量文件和服务启动命令
[root@e0e8f8596277 /]# cat /lib/systemd/system/httpd.service 
........
[Service]
EnvironmentFile=/etc/sysconfig/httpd		#环境变量文件
ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND		#启动命令，$OPTIONS 此环境变量为空，可以不用写
[root@e0e8f8596277 /]# cat /etc/sysconfig/httpd		#从环境变量文件中，获取环境变量
......
LANG=C
[root@e0e8f8596277 /]# export LANG=C		#设置语言，避免出现不必要的字符集问题，不然容易出现乱码
[root@e0e8f8596277 /]# /usr/sbin/httpd -DFOREGROUND		#启动httpd服务，在前台运行
AH00558: httpd: Could not reliably determine the server's fully qualified domain name, using 172.17.0.2. Set the 'ServerName' directive globally to suppress this message

另外开一个docker-0002终端，通过提示的地址172.17.0.2
[root@docker-0002 ~]# curl 172.17.0.2
hello world

此时容器内容的终端是被占用的，是在前台运行的，如果关闭，不能够再次访问httpd，若想要退出，使用ctrl + pq
[root@docker-0002 ~]# docker exec -it myapache /bin/bash
[root@e0e8f8596277 /]# ps -ef
UID        PID  PPID  C STIME TTY          TIME CMD
root         1     0  0 13:17 pts/0    00:00:00 /bin/bash
root        27     1  0 13:20 pts/0    00:00:00 /usr/sbin/httpd -D
apache      28    27  0 13:20 pts/0    00:00:00 /usr/sbin/httpd -D
apache      29    27  0 13:20 pts/0    00:00:00 /usr/sbin/httpd -D
apache      30    27  0 13:20 pts/0    00:00:00 /usr/sbin/httpd -D
apache      31    27  0 13:20 pts/0    00:00:00 /usr/sbin/httpd -D
apache      32    27  0 13:20 pts/0    00:00:00 /usr/sbin/httpd -D
root        54     0  0 13:27 pts/1    00:00:00 /bin/bash
root        76    54  0 13:27 pts/1    00:00:00 ps -ef
[root@e0e8f8596277 /]# exit
```



# 补充

~~~shell
名称空间：  
	是指可以对系统资源空间进行分割隔离的技术，例如：创建一个虚拟机，在虚拟机里的所有操作，都不会对真实机造成影响。

名称空间分为六大类，可以从各个方面来对系统资源空间进行隔离；
	UTS、NETWORK、MOUNT、USER、PID、IPC

UTS：   
   #主机名，作用：分割主机名，即在容器内修改主机名，不会对宿主机的系统造成影响，实现主机名的隔离；

NETWORK：网络，作用：分割网络，即容器内的网络配置和宿主机之间相互不受干扰；
   例如：
       #在真实机器上的网卡名为ens33,IP地址为192.168.1.10/24；
       #而在容器内的网卡名可以为eth0，ip地址为10.10.10.10/24；     

MOUNT:
   #挂载，作用：隔离文件系统，在容器内挂载的光盘，宿主机是无法看到里面的内容的；
   例如：
       #在linux系统上，创建一个虚拟机，在真机的/var/lib/ftp中挂载了一个光盘文件，但是在虚拟机的/var/lib/ftp中是没有光盘内容的，这就是MOUNT隔离；

USER:
   #用户，作用：隔离用户，即容器内创建的用户不能用于登录宿主机，真机机里创建的用户也不能作用于容器；

PID:
   #进程，作用：为了防止容器和宿主机中的进程冲突；
   例如：
       #在真实机中，有一个服务: nfs，PID为2250；
       #在容器内，也有一个服务: chrony，PID为2250；
       #真实机中用户，杀死(kill) PID号为2250的进程时，并不会对容器内的进程2250产生影响；
       #而容器内的用户，杀死(kill) PID号为2250的进程时，也并不会对真实机内的进程2250产生影响；

IPC:
   #信号向量，作用：通常和PID一起使用；
   #用户杀死一个进程时，实际上是向进程发送一个信号(IPC)，进程接收到这个信号后会执行对应的操作；
~~~
