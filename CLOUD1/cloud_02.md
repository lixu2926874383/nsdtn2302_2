# 公有云

[toc]

## 云平台部署管理架构图

```mermaid
flowchart LR
  subgraph L1[公有云平台]
    EIP((公网IP)) o--o P[(跳板机)] --> A1[(云主机)] & A2[(云主机)]
    M{{私有镜像}}
  end
U((管理员)) -..-> EIP
classDef Cloud fill:#ccccff
class L1 Cloud
classDef Jumpserver fill:#f0f090,color:#0000ff,stroke:#f66,stroke-width:2px
class P,M,A1,A2,EIP Jumpserver
classDef User fill:#90f0f0,stroke:#0000ff,stroke-width:2px
class U User
```

## 公有云配置

区域： 同一个区域中的云主机是可以互相连通的，不通区域云主机是不能使用内部网络互相通信的  
选择离自己比较近的区域，可以减少网络延时卡顿  

## 创建云主机

创建虚拟私有云vpc

![image-20230606000033925](cloud_01.assets/image-20230606000033925.png)

![image-20230606000059053](cloud_01.assets/image-20230606000059053.png)

![image-20230606000237070](cloud_01.assets/image-20230606000237070.png)

![image-20230606000220065](cloud_01.assets/image-20230606000220065.png)



设置安全组，defaults默认安全组ipv4地址和ipv6地址放行所有

![image-20230606000422948](cloud_01.assets/image-20230606000422948.png)

![image-20230606000901039](cloud_01.assets/image-20230606000901039.png)

创建云主机，名称为ecs-proxy，规格为2CPU/4G内存，ip地址为192.168.1.252

![image-20230606001907036](cloud_01.assets/image-20230606001907036.png)

![image-20230606004205295](cloud_01.assets/image-20230606004205295.png)

![image-20230606004500074](cloud_01.assets/image-20230606004500074.png)

![image-20230606004743731](cloud_01.assets/image-20230606004743731.png)

![image-20230606004907001](cloud_01.assets/image-20230606004907001.png)

![image-20230606011301570](cloud_01.assets/image-20230606011301570.png)

![image-20230606005027206](cloud_01.assets/image-20230606005027206.png)

![image-20230606005203133](cloud_01.assets/image-20230606005203133.png)

![image-20230606005220705](cloud_01.assets/image-20230606005220705.png)

![image-20230606005242174](cloud_01.assets/image-20230606005242174.png)

![image-20230606005259242](cloud_01.assets/image-20230606005259242.png)

查看结果

![image-20230606005403886](cloud_01.assets/image-20230606005403886.png)







此时ecs-proxy（192.168.1.252）云主机已经购买出来，按照购买ecs-proxy的步骤，再次购买一台云主机，规格为1CPU/1G内存，ip为192.168.1.251，名称为ecs-host









云主机的远程管理：通过华为云的页面管理、ssh远程连接

华为云页面管理，方法一：

![image-20230606011548443](cloud_01.assets/image-20230606011548443.png)

![image-20230606011629631](cloud_01.assets/image-20230606011629631.png)

华为云页面管理，方法二：

![image-20230606011757520](cloud_01.assets/image-20230606011757520.png)

![image-20230606011727878](cloud_01.assets/image-20230606011727878.png)

实验练习：

​	1、使用ecs-proxy主机ping ecs-host测试是否能够连通

​	2、在ecs-host主机中手动更改ip为192.168.1.125，再次测试

~~~shell
[root@ecs-host ~]# ifconfig eth0 192.168.1.125/24
[root@ecs-host ~]# ifconfig						#地址已经更改
~~~

​	3、测试失败，ecs-proxy ping 不通 ecs-host 主机，ecs-host主机需要关机在页面点击更改ip才行

![image-20230606013019311](cloud_01.assets/image-20230606013019311.png)

![image-20230606013044819](cloud_01.assets/image-20230606013044819.png)

​	4、开机ecs-host，再次测试

ssh远程连接云主机

![image-20230606014653048](cloud_01.assets/image-20230606014653048.png)

![image-20230606014707446](cloud_01.assets/image-20230606014707446.png)

![image-20230606014758084](cloud_01.assets/image-20230606014758084.png)

![image-20230606014820028](cloud_01.assets/image-20230606014820028.png)

![image-20230606014832534](cloud_01.assets/image-20230606014832534.png)

给ecs-proxy云主机绑定公网ip

![image-20230606014902713](cloud_01.assets/image-20230606014902713.png)

![image-20230606014946262](cloud_01.assets/image-20230606014946262.png)

![image-20230606015004878](cloud_01.assets/image-20230606015004878.png)

使用远程工具通过公网ip连接云主机

![image-20230606015304994](cloud_01.assets/image-20230606015304994.png)

## 跳板机配置

### 配置yum源，安装工具包
```shell
华为云yum私有源：https://support.huaweicloud.com/sms_faq/sms_faq_0038.html  
华为云yum公有源：https://mirrors.huaweicloud.com/home

[root@ecs-proxy ~]# rm -rf /etc/yum.repos.d/*.repo
[root@ecs-proxy ~]# curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.myhuaweicloud.com/repo/CentOS-Base-7.repo
[root@ecs-proxy ~]# yum clean all
[root@ecs-proxy ~]# yum install -y net-tools lftp rsync psmisc vim-enhanced tree vsftpd  bash-completion createrepo lrzsz iproute
[root@ecs-proxy ~]# systemctl enable --now vsftpd
[root@ecs-proxy ~]# mkdir -p /var/ftp/localrepo
[root@ecs-proxy ~]# createrepo  /var/ftp/localrepo
```
### 优化系统服务
```shell
[root@ecs-proxy ~]# systemctl stop postfix
[root@ecs-proxy ~]# yum remove -y postfix at audit kexec-tools firewalld-*
#at：计划任务工具，可以在指定的时间执行一次性任务
#kexec-tools：一种工具，用于在不重启系统的情况下加载一个新的内核镜像。

[root@ecs-proxy ~]# vim /etc/cloud/cloud.cfg		#华为云初始化程序配置文件
 19 #manage_etc_hosts: localhost	#不自动修改hosts文件

[root@ecs-proxy ~]# vim /etc/hosts
...
127.0.0.1       ecs-proxy        ecs-proxy	#删除此行
[root@ecs-proxy ~]# reboot
```
### 配置ansible管理主机
```shell
从真机上传ansible_centos7.tar.gz 到ecs-proxy主机的root目录下
[root@ecs-proxy ~]# tar zxf ansible_centos7.tar.gz
[root@ecs-proxy ~]# yum install -y ansible/*.rpm

配置免密登录,能够免密登录其他机器
[root@ecs-proxy ~]# ssh-keygen 		#一路回车
[root@ecs-proxy ~]# ssh-copy-id 192.168.1.125
```
## 模板镜像配置

### 配置yum源，安装工具包
```shell
[root@ecs-proxy ~]# ssh 192.168.1.125
[root@ecs-host ~]# rm -rf /etc/yum.repos.d/*.repo
[root@ecs-host ~]# curl -o /etc/yum.repos.d/CentOS-Base.repo http://mirrors.myhuaweicloud.com/repo/CentOS-Base-7.repo

配置自定义的yum源
[root@ecs-host ~]# vim /etc/yum.repos.d/local.repo 
[local_repo]
name=CentOS-$releasever - Localrepo
baseurl=ftp://192.168.1.252/localrepo
enabled=1
gpgcheck=0
[root@ecs-host ~]# yum clean all
[root@ecs-host ~]# yum repolist
[root@ecs-host ~]# yum install -y net-tools lftp rsync psmisc vim-enhanced tree lrzsz bash-completion iproute

ecs-host主机也需要优化系统服务
[root@ecs-host ~]# systemctl stop postfix
[root@ecs-host ~]# yum remove -y postfix at audit kexec-tools firewalld-*
[root@ecs-host ~]# vim /etc/cloud/cloud.cfg		#华为云初始化程序配置文件
 19 #manage_etc_hosts: localhost	#不自动修改hosts文件

[root@ecs-host ~]# vim /etc/hosts
...
127.0.0.1       ecs-host        ecs-host	#删除此行
[root@ecs-host ~]# yum clean all #清理缓存，做的镜像更小
[root@ecs-host ~]# poweroff
```

<b><font color=#ff0000 size="5">关机以后把主机系统盘制作为模板</font></b>

![image-20230606032201517](cloud_01.assets/image-20230606032201517.png)

![image-20230606032240228](cloud_01.assets/image-20230606032240228.png)

环境准备：把五阶段的软件包/linux-soft/5 拷贝的到ecs-proxy的跳板机上面

```shell
1、linux环境：真机执行：rsync -av --delete /linux-soft/5  跳板机公网ip:/root/
2、windows环境，直接远程软件连接跳板机ecs-proxy，windows主机上软件包打包压缩，上传
```
