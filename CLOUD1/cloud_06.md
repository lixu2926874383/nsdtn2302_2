## 容器技术

[toc]

## 镜像编排

### Dockerfile语法

| 语法指令 | 语法说明                              |
| -------- | ------------------------------------- |
| FROM     | 基础镜像                              |
| RUN      | 制作镜像时执行的命令，可以有多个      |
| ADD      | 复制文件到镜像，自动解压              |
| COPY     | 复制文件到镜像，不解压                |
| EXPOSE   | 声明开放的端口                        |
| ENV      | 设置容器启动后的环境变量              |
| WORKDIR  | 定义容器默认工作目录（等于cd）        |
| CMD      | 容器启动时执行的命令，仅可以有一条CMD |


### 制作apache镜像

httpd.service 文件路径：/lib/systemd/system/httpd.service   

```dockerfile
[root@docker-0002 ~]# mkdir /root/apache
[root@docker-0002 ~]# cd /root/apache

拷贝动态页面到docker-0002的/root/apache/
[root@ecs-proxy ~]# scp /root/5/public/info.php 192.168.1.32:/root/apache/

[root@docker-0002 apache]# echo 'Welcome to The Apache.' > index.html
[root@docker-0002 apache]# tar czf myweb.tar.gz index.html info.php
[root@docker-0002 apache]# vim Dockerfile
FROM mycentos:latest
RUN  yum install -y httpd php && yum clean all
ENV  LANG=C
ADD  myweb.tar.gz /var/www/html/
WORKDIR /var/www/html/
EXPOSE 80
CMD  ["/usr/sbin/httpd", "-DFOREGROUND"]
[root@docker-0002 apache]# docker build -t myapache:latest .
```

#### 验证镜像

```shell
[root@docker-0002 apache]# docker images		#可以看到有myapache的镜像
[root@docker-0002 apache]# docker rm -f $(docker ps -aq)
[root@docker-0002 apache]# docker run -itd --name myhttpd myapache:latest
[root@docker-0002 apache]# curl 172.17.0.2
Welcome to The Apache.
[root@docker-0002 apache]# docker rm -f myhttpd
myhttpd
```


### 制作phpfpm镜像

```dockerfile
#先手工做一遍
[root@docker-0002 apache]# docker run -it mycentos:latest 
[root@dfd4e3d6dd96 /]# yum -y install php-fpm

#找service文件
[root@dfd4e3d6dd96 /]# cat /lib/systemd/system/php-fpm.service 
...
ExecStart=/usr/sbin/php-fpm --nodaemonize
...
[root@dfd4e3d6dd96 /]# /usr/sbin/php-fpm --nodaemonize		#前台启动，ctrl + C 终止


[root@docker-0002 apache]# mkdir /root/phpfpm
[root@docker-0002 apache]# cd /root/phpfpm
[root@docker-0002 phpfpm]# vim Dockerfile
FROM mycentos:latest
RUN  yum install -y php-fpm && yum clean all
EXPOSE 9000
CMD ["/usr/sbin/php-fpm", "--nodaemonize"]
[root@docker-0002 phpfpm]# docker build -t phpfpm:latest .
```

#### 验证镜像

```shell
启动php-fpm的容器，查看监听端口
[root@docker-0002 phpfpm]# docker images	#有phpfpm的镜像
[root@docker-0002 phpfpm]# docker run -itd --name myphp phpfpm:latest
[root@docker-0002 phpfpm]# docker exec -it myphp ss -antlp
Netid State      Recv-Q Send-Q       Local Address:Port      Peer Address:Port
tcp   LISTEN     0      128              127.0.0.1:9000                 *:*
[root@docker-0002 phpfpm]# docker rm -f myphp
```

### 制作nginx镜像

#### 编译软件包

```shell
从ecs-proxy主机拷贝nginx的软件包到docker-0002主机
[root@ecs-proxy ~]# scp /root/5/public/nginx-1.17.6.tar.gz  192.168.1.32:/root/
[root@docker-0002 ~]# useradd nginx
[root@docker-0002 ~]# yum install -y gcc make pcre-devel openssl-devel
[root@docker-0002 ~]# tar zxf nginx-1.17.6.tar.gz
[root@docker-0002 ~]# cd nginx-1.17.6/
[root@docker-0002 nginx-1.17.6]# ./configure --prefix=/usr/local/nginx --user=nginx --group=nginx --with-http_ssl_module
[root@docker-0002 nginx-1.17.6]# make && make install
[root@docker-0002 nginx-1.17.6]# echo 'Nginx is running !'>/usr/local/nginx/html/index.html
[root@docker-0002 nginx-1.17.6]# cd /usr/local
[root@docker-0002 nginx-1.17.6]# tar -czf nginx.tar.gz nginx
```

#### 制作镜像


```dockerfile
[root@docker-0002 ~]# mkdir /root/nginx
[root@docker-0002 ~]# cd /root/nginx
[root@docker-0002 nginx]# cp /usr/local/nginx.tar.gz ./
[root@docker-0002 nginx]# vim Dockerfile 
FROM mycentos:latest
RUN  yum install -y pcre openssl && useradd nginx && yum clean all
ADD  nginx.tar.gz /usr/local/
WORKDIR /usr/local/nginx/html
EXPOSE 80
CMD  ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]
[root@docker-0002 nginx]# docker build -t mynginx:latest .
```

#### 验证镜像

```shell
[root@docker-0002 nginx]# docker images		#有mynginx的镜像
[root@docker-0002 nginx]# docker run -itd --name mynginx mynginx:latest
[root@docker-0002 nginx]# curl http://172.17.0.2/
Nginx is running !
[root@docker-0002 nginx]# docker rm -f mynginx
```

## 微服务

### 对外发布服务

docker  run  -itd  -p 宿主机端口:容器端口  镜像名称:标签

```shell
# 把 docker-0002 绑定 apache 服务
[root@docker-0002 ~]# docker run -itd -p 80:80 myapache:latest

# 在 ecs-proxy主机 上访问验证
[root@ecs-proxy ~]# curl 192.168.1.32
Welcome to The Apache.

# 把 docker-0002 绑定 nginx 服务，删除 apache 的容器
[root@docker-0002 ~]# docker rm -f $(docker ps -aq)
[root@docker-0002 ~]# docker run -itd -p 80:80 mynginx:latest

# 在 ecs-proxy主机 上访问验证
[root@ecs-proxy ~]# curl 192.168.1.32
Nginx is running !
```

### 容器共享卷

`docker run -itd -v 宿主机对象:容器内对象 镜像名称:标签  `

#### 共享网页目录

```shell
[root@docker-0002 ~]# mkdir /var/webroot
[root@docker-0002 ~]# echo "hello world" >> /var/webroot/index.html
[root@docker-0002 ~]# cp /root/apache/info.php /var/webroot/
[root@docker-0002 ~]# docker rm -f $(docker ps -aq)
[root@docker-0002 ~]# docker run -itd --name mynginx -v /var/webroot:/usr/local/nginx/html mynginx:latest 
[root@docker-0002 ~]# curl http://172.17.0.2
hello world

测试：
[root@docker-0002 ~]# echo "hello world" >> /var/webroot/index.html
[root@docker-0002 ~]# echo "hello world" >> /var/webroot/index.html
[root@docker-0002 ~]# curl 172.17.0.2
hello world
hello world
hello world

[root@docker-0002 ~]# docker run -itd --name myhttpd -v /var/webroot:/var/www/html myapache:latest
[root@docker-0002 ~]# curl 172.17.0.3
hello world
hello world
hello world
[root@docker-0002 ~]# curl 172.17.0.3/info.php	#apache正常解析动态页面
<pre>
Array
(
    [REMOTE_ADDR] => 172.17.0.1
    [REQUEST_METHOD] => GET
    [HTTP_USER_AGENT] => curl/7.29.0
    [REQUEST_URI] => /info.php
)
php_host:       c73bc24c455f
1229

```

#### 修改nginx的配置文件

```shell
nginx则不行，需要另行配置解析php页面
[root@docker-0002 ~]# mkdir /var/webconf
[root@docker-0002 ~]# docker cp mynginx:/usr/local/nginx/conf/nginx.conf /var/webconf/
[root@docker-0002 ~]# vim /var/webconf/nginx.conf
 65         location ~ \.php$ {
 66             root           html;
 67             fastcgi_pass   127.0.0.1:9000;
 68             fastcgi_index  index.php;
 69             include        fastcgi.conf;
 70         }
 
[root@docker-0002 ~]# docker rm -f $(docker ps -aq)
[root@docker-0002 ~]# docker run -itd --name mynginx -v /var/webconf/nginx.conf:/usr/local/nginx/conf/nginx.conf -v /var/webroot:/usr/local/nginx/html mynginx:latest

[root@docker-0002 ~]# curl 172.17.0.2/info.php	#显示404 Not Found

通过连接nginx的容器查看fastcgi转发127.0.0.1:9000失败，nginx中没有9000
[root@docker-0002 ~]# docker exec -it mynginx /bin/bash
[root@0214c6744ffa html]# cat ../logs/error.log 
2023/06/12 17:10:08 [error] 7#0: *6 open() "/usr/local/nginx/html/50x.html" failed (2: No such file or directory), client: 172.17.0.1, server: localhost, request: "GET /info.php HTTP/1.1", upstream: "fastcgi://127.0.0.1:9000", host: "172.17.0.2"
```

### 容器间网络通信

实验架构图例

```mermaid
flowchart LR
  subgraph docker-0002
      subgraph 容器1
        APP1[(Nginx)] & NET((共享网卡)) 
      end
      subgraph 容器2
        APP2[(PHP)]
      end
    APP1 & APP2 o---o NET((共享网卡)) & L((共享存储卷))
  end
U((用户)) --> APP1
classDef Docker fill:#ffffc0,color:#ff00ff
class docker-0002 Docker
classDef Container fill:#88aaff,color:#00ff00
class 容器1,容器2 Container
classDef DEV fill:#0f0fc0,color:#f0f000
class L DEV
```

实验步骤

```shell
演示共享其他容器的网络命名空间
在docker-0002新开两个终端，第一个终端执行以下命令
[root@docker-0002 ~]# docker run  -it --name a1 mycentos:latest 
[root@8366137bda56 /]# 

第二个终端执行，启动容器，共享a1的网络
[root@docker-0002 ~]# docker run -it --network=container:a1 mycentos:latest

验证网络共享：
此时使用ifconfig查看网卡，他们的网卡ip是一样的

在第一个终端的容器安装httpd，第二个查看，可以看到httpd的端口
[root@8366137bda56 /]# yum -y install httpd
[root@8366137bda56 /]# /usr/sbin/httpd
[root@8366137bda56 /]# ss -antlp			#有端口

第二个终端查看
[root@8366137bda56 /]# ss -antlp			#有端口

第一个终端杀死程序，查看监听端口，已经没有了
[root@8366137bda56 /]# killall  httpd


把nginx和php-fpm共享网络
[root@docker-0002 ~]# docker run -itd --name myphp --network=container:mynginx -v /var/webroot:/usr/local/nginx/html phpfpm:latest
[root@docker-0002 ~]# docker exec -it mynginx ss -antlp
Netid  State      Recv-Q    Send-Q     Local Address:Port
tcp    LISTEN     0         128            127.0.0.1:9000
tcp    LISTEN     0         128                    *:80
[root@docker-0002 ~]# curl 172.17.0.2/info.php
<pre>
Array
(
    [REMOTE_ADDR] => 172.17.0.1
    [REQUEST_METHOD] => GET
    [HTTP_USER_AGENT] => curl/7.29.0
    [REQUEST_URI] => /info.php
)
php_host:       f705f89b45f9
1229
```

## docker私有仓库

### 私有仓库图例

```mermaid
flowchart LR
H1 o-..-o R([互联网仓库]):::Deploy
subgraph C[本地集群]
  I((本地仓库)):::Deploy
  H1[(容器服务<br>docker-0001)] & H2[(容器服务<br>docker-0002)] --> I
end
classDef Cluster fill:#ffffc0,color:#ff00ff,stroke-width:4px
class C Cluster
classDef Pod fill:#ccffbb,color:#000000,stroke-width:3px
class H1,H2 Pod
classDef Deploy fill:#40aff0,color:#f0ff00,stroke:#f06080,stroke-width:3px  
```

### 私有仓库配置

| 主机名   | ip地址       | 最低配置    |
| -------- | ------------ | ----------- |
| registry | 192.168.1.30 | 2CPU,4G内存 |

```shell
[root@registry ~]# yum install -y docker-distribution
[root@registry ~]# systemctl enable --now docker-distribution

验证结果：
[root@registry ~]# curl http://192.168.1.30:5000/v2/_catalog
{"repositories":[]}
```

### 客户端配置

所有node节点都需要配置，这里 docker-0001，docker-0002都要配置

```shell
配置主机名解析
[root@docker-0001 ~]# vim /etc/hosts
192.168.1.30	registry

"insecure-registries"：声明这个仓库是不安全的，仍然使用
[root@docker-0001 ~]# vim /etc/docker/daemon.json
{
    "exec-opts": ["native.cgroupdriver=systemd"],
    "registry-mirrors": ["http://registry:5000","https://hub-mirror.c.163.com"],
    "insecure-registries":["registry:5000"]
}
[root@docker-0001 ~]# docker rm -f $(docker ps -aq)
[root@docker-0001 ~]# systemctl restart docker

docker-0002配置
[root@docker-0001 ~]# scp /etc/hosts 192.168.1.32:/etc/
[root@docker-0001 ~]# scp /etc/docker/daemon.json 192.168.1.32:/etc/docker/
[root@docker-0002 ~]# systemctl restart docker
```

#### 上传镜像

```shell
[root@docker-0001 ~]# docker tag centos:7 registry:5000/centos:7
[root@docker-0001 ~]# docker push registry:5000/centos:7

上传所有的myos镜像，可以写个循环
[root@docker-0001 ~]# for i in nginx phpfpm httpd latest v2009 
 do
 docker tag myos:$i registry:5000/myos:$i
 docker push registry:5000/myos:$i
 docker rmi registry:5000/myos:$i
 docker rmi myos:$i
 done
```

#### 验证测试

查看镜像名称： curl http://仓库IP:5000/v2/_catalog  
查看镜像标签： curl http://仓库IP:5000/v2/镜像名称/tags/list

```shell
#查看镜像名称
[root@docker-0002 ~]# curl http://registry:5000/v2/_catalog
{"repositories":["centos","myos"]}

#查看镜像标签
[root@docker-0002 ~]# curl http://registry:5000/v2/myos/tags/list
{"name":"myos","tags":["phpfpm","nginx","v2009","httpd","latest"]}

#下载镜像
[root@docker-0002 ~]# docker pull registry:5000/myos:nginx

#直接启动容器，没有镜像会直接自行下载
[root@docker-0002 ~]# docker run -itd registry:5000/myos:httpd
```

