# ELK日志分析平台

[toc]

## Kibana测试使用

浏览器访问kibana：负载均衡IP:5601

![image-20230607005606908](cloud_01.assets/image-20230607005606908.png)

选择 自己浏览 ，配置数据

在kibana中指定索引，绘制图表

![image-20230608002407818](cloud_01.assets/image-20230608002407818.png)

![image-20230608002737056](cloud_01.assets/image-20230608002737056.png)

![image-20230608005331766](cloud_01.assets/image-20230608005331766.png)

已经可以看到数据

![image-20230608005400858](cloud_01.assets/image-20230608005400858.png)

也可以自己做其他的图表数据

![image-20230608005442730](cloud_01.assets/image-20230608005442730.png)

![image-20230608005456845](cloud_01.assets/image-20230608005456845.png)

![image-20230608005508959](cloud_01.assets/image-20230608005508959.png)

![image-20230608005525945](cloud_01.assets/image-20230608005525945.png)



选择 词，即在日志中记录的字段；选择具体字段，如geo.src.keyword(用户的来源地)

![image-20230608005603977](cloud_01.assets/image-20230608005603977.png)

也可以禁用

![image-20230608005715738](cloud_01.assets/image-20230608005715738.png)

选择machine.os.keyword(操作系统）

![image-20230608005737380](cloud_01.assets/image-20230608005737380.png)

![image-20230608005816995](cloud_01.assets/image-20230608005816995.png)

也可以选择agent.keyword(浏览器)，自行做实验

## ELK架构图例

```mermaid
flowchart LR
subgraph Z1[web cluster]
    H1([apache]) o--o F1([filebeat]);H2([apache]) o--o F2([filebeat]);H3([apache]) o--o F3([filebeat])
end
subgraph Z2[Logstash]
  F1 & F2 & F3 --> A1((input)) ==> A2((filter)) ==> A3((output))
end
subgraph Z3[Elasticsearch]
  ES1(es-0001);ES2(es-0002);ES3(es-0003);ES4(es-0004);ES5(es-0005)
end
A3 --> ES1 & ES2 & ES3 & ES4 & ES5 --> K[(kibana)]
classDef APP color:#0000ff,fill:#99ff99
class H1,H2,H3,F1,F2,F3,ES1,ES2,ES3,ES4,ES5,A1,A2,A3,K APP
classDef ZONE fill:#ffffc0,color:#ff00ff
class Z1,Z2,Z3 ZONE
```

## logstash安装

### 购买云主机 

| 主机     | IP地址       | 配置          |
| -------- | ------------ | ------------- |
| logstash | 192.168.1.27 | 最低配置4核8G |

### 安装logstash

```shell
[root@ecs-proxy ~]# ssh 192.168.1.27
[root@logstash ~]# vim /etc/hosts
192.168.1.21	es-0001
192.168.1.22	es-0002
192.168.1.23	es-0003
192.168.1.24	es-0004
192.168.1.25	es-0005
192.168.1.27	logstash
[root@logstash ~]# yum install -y java-1.8.0-openjdk-devel logstash
```

### 基础配置样例

```shell
[root@logstash ~]# ln -s /etc/logstash /usr/share/logstash/config	# #logstash安装时配置在 /usr/share/logstash/，但是红帽安装时配置放到了/etc/，需要做个软连接到/usr/share/logstash/config，不然logstash找不到配置文件

logstash 配置文件编写，stdin{} 和 stdout{} 都是logstash的插件；，logstash 的管理都是基于插件来完成的；

编写logstash的配置文件
#stdin {}   为标准输入，即键盘和鼠标
#stdout {}  为标准输出，即显示器

[root@logstash ~]# vim /etc/logstash/conf.d/my.conf		#配置文件以.conf结尾
input { 
  stdin {}
}

filter{ }

output{ 
  stdout{}
}
[root@logstash ~]# /usr/share/logstash/bin/logstash
afsddddddddddd		#输入普通字符串
/usr/share/logstash/vendor/bundle/jruby/2.5.0/gems/awesome_print-1.7.0/lib/awesome_print/formatters/base_formatter.rb:31: warning: constant ::Fixnum is deprecated
{
      "@version" => "1",
    "@timestamp" => 2023-06-07T17:30:24.178Z,
       "message" => "afsddddddddddd",
          "host" => "logstash"
}
{"a":"1", "b":"2", "c":"3"}		#输入json格式的数据，解析失败
{
      "@version" => "1",
    "@timestamp" => 2023-06-07T17:30:31.678Z,
       "message" => "{\"a\":\"1\", \"b\":\"2\", \"c\":\"3\"}",
          "host" => "logstash"
}

直接ctrl + C 退出

使用插件
  - [x] 上面的配置文件使用了  logstash-input-stdin  和 logstash-output-stdout 两个插件，logstash 对数据的处理依赖插件
插件管理
查看logstash插件的使用命令
[root@logstash ~]# /usr/share/logstash/bin/logstash-plugin --help

列出logstash的所有插件，logstash的插件由三部分组成,以 - 进行分隔：
[root@logstash ~]# /usr/share/logstash/bin/logstash-plugin list
#第一部分：代表logstash的插件；
#第二部分：表示该插件只能用于logstash的哪一部分:
          例如：
              #-input- 则只能用于读取数据；
              #-output- 只能用于输入数据到某个设备或文件；
              #-filter- 用于对数据进行分析和处理； 
              #-codec-  为编码插件，可以用于任何的区域【input,output或filter】；
#第三部分：表示该插件具体的作用；
```

### 插件与调试格式

使用json格式字符串测试  {"a":"1", "b":"2", "c":"3"}

```shell
使用编码类插件，rubydebug调试格式时，以易读的格式显示
stdin { codec => "json" } : 输入json格式的数据
stdout{ codec => "rubydebug" } ： 数据输出以易读的格式显示

[root@logstash ~]# vim /etc/logstash/conf.d/my.conf
input { 
  stdin { codec => "json" }
}

filter{ }

output{ 
  stdout{ codec => "rubydebug" }
}
[root@logstash ~]# /usr/share/logstash/bin/logstash
{"a":"1", "b":"2", "c":"3"}		#输入json格式的数据,解析成功
/usr/share/logstash/vendor/bundle/jruby/2.5.0/gems/awesome_print-1.7.0/lib/awesome_print/formatters/base_formatter.rb:31: warning: constant ::Fixnum is deprecated
{
      "@version" => "1",
    "@timestamp" => 2023-06-07T15:42:42.603Z,
             "b" => "2",
             "c" => "3",
          "host" => "logstash",
             "a" => "1"
}
```

#### input file插件：从文件中读取数据

官方手册地址：https://www.elastic.co/guide/en/logstash/current/index.html

找6.8版本的logstash，或者相近版本的

![image-20230608015241492](cloud_01.assets/image-20230608015241492.png)

![image-20230608015342605](cloud_01.assets/image-20230608015342605.png)

找到案例，可以直接复制

![image-20230608015524723](cloud_01.assets/image-20230608015524723.png)



```shell
定义input区域，将查找到的file插件代码，写入到input区域中
在file的帮助文档中查找，file插件中 input 区域的配置
setting         #指的是key【键】，定义时名字必须相同；
input type      #value【值】的数据类型；
Required        #如果为yes,则为使用file插件的必须设置的键值；如果为no，则可有可无；

file插件的帮助文档中，找到一个必须配置path，要写入到配置文件中
定义file 插件的必须配置，path 指的是文件读取的路径，数组类型，可以同时读取多个文件的数据
[root@logstash ~]# vim /etc/logstash/conf.d/my.conf
input{
  stdin{ codec => "json" }
  file {
    path => ["/tmp/a.log","/tmp/b.log"]
  }
 }
......

验证配置,创建日志文件，启动logstash服务
[root@logstash ~]# /usr/share/logstash/bin/logstash


重开一个logstash终端，写入日志数据
echo A_${RANDOM}  每次都会输出不同的数据【RANDOM】为随机数的环境变量，写入数据后，回到logstash服务终端下查看，会有对应的数据输出
[root@logstash ~]# echo  A_${RANDOM} >> /tmp/a.log 
[root@logstash ~]# echo  B_${RANDOM} >> /tmp/b.log 

在启动logstash服务的终端查看数据输出结果 

file 插件常用参数
type => 'testlog'		#type 提供一个字符串标记

type字符串标记的使用,对来自不同服务的日志打标签，区别日志来源
[root@logstash ~]# vim /etc/logstash/conf.d/my.conf
input{
  stdin{ codec => "json" }
  file {
    path => ["/tmp/a.log"] 
    type => "weblog"
  }
  file {
    path => ["/tmp/b.log"]
    type => "dblog"
  }
 }
......
[root@logstash ~]# /usr/share/logstash/bin/logstash

重开一个终端，写入日志数据
[root@logstash ~]# echo  A_${RANDOM} >> /tmp/a.log 
[root@logstash ~]# echo  B_${RANDOM} >> /tmp/b.log 

在启动logstash服务的终端查看数据输出结果

file模块中，start_position和 sincedb_path 的使用 
start_position =>  "beginning"		#start_position第一次读取文件位置 [beginning文件开始读取 | end，丢弃之前的日志，不在读取];
sincedb_path =>  "/var/lib/logstash/sincedb-access"		#sincedb_path 记录读取文件的位置，让logstash每次读取数据时，都是继续上次的位置，继续读取日志数据

创建日志文件，添加数据
[root@logstash ~]# vim /etc/logstash/conf.d/my.conf
input{
  stdin{ codec => "json" }
  file {
    path => ["/tmp/c.log"]
    type => "weblog"
  }
 }
......

[root@logstash ~]# echo  C_${RANDOM} >> /tmp/c.log 
[root@logstash ~]# echo  C_${RANDOM} >> /tmp/c.log 

此时启动配置文件，数据是读取不到的
[root@logstash ~]# /usr/share/logstash/bin/logstash

修改配置文件
[root@logstash ~]# vim /etc/logstash/conf.d/my.conf	
input{
  stdin{ codec => "json" }
  file {
    path => ["/tmp/c.log"]
    type => "weblog"
    start_position => "beginning"
    sincedb_path =>  "/var/lib/logstash/sincedb"
  }
 }
......

使用配置文件启动logstash服务,输出c.log中之前的日志数据
[root@logstash ~]# /usr/share/logstash/bin/logstash
ctrl+c 停止logstash的服务

[root@logstash ~]# echo  A_${RANDOM} >> /tmp/c.log 	向c.log中继续添加数据
[root@logstash ~]# echo  A_${RANDOM} >> /tmp/c.log 

在启动logstash服务查看输出结果，是接着上次的位置继续读取的
[root@logstash ~]# /usr/share/logstash/bin/logstash
```

#### filter grok插件

> 正则表达式分组匹配格式: (?<名字>正则表达式)
> 正则表达式宏调用格式: %{宏名称:名字}
> 宏文件路径 :
> /usr/share/logstash/vendor/bundle/jruby/2.5.0/gems/logstash-patterns-core-4.1.2/patterns

```shell
编写logstash中的filter区域，logstash的配置文件，方便filter区域的调试

环境准备：
[root@logstash ~]# vim /etc/logstash/conf.d/my.conf
input{
  stdin{ codec => "json" }
  file {
    path => ["/tmp/c.log"]
    type => "weblog"
    start_position => "beginning"  
    #sincedb_path => "/var/lib/logstash/sincedb"	#取消记录下次读取日志位置文件
    sincedb_path => "/dev/null"					#将记录位置定义为黑洞设备
  }
}
...

修改c.log日志文件，便于输出查看，只留一条apache日志记录
[root@logstash ~]# echo '192.168.1.252 - - [29/Jul/2022:14:06:57 +0800] "GET /info.html HTTP/1.1" 200 119 "-" "curl/7.29.0"' >/tmp/c.log


采用的logstash提供的宏来匹配过滤IP地址
[root@logstash ~]# vim /etc/logstash/conf.d/my.conf
......
filter{
      grok {
        match => { "message" => "%{IP:client_ip}" }
      }
}
......

启动logstash服务，查看匹配结果
[root@logstash ~]# /usr/share/logstash/bin/logstash		#客户端IP地址匹配出来


采用的logstash提供的宏来匹配apache日志格式；remove_field：删除message的信息
[root@logstash ~]# vim /etc/logstash/conf.d/my.conf
......
filter{
  grok {
    match => { "message" => "%{HTTPD_COMBINEDLOG}" }
    remove_field => ["message"]
  }
}
......

启动logstash服务，查看匹配结果
[root@logstash ~]# /usr/share/logstash/bin/logstash
...
{
      "@version" => "1",
    "@timestamp" => "2022-10-31T03:33:49.240Z",
     "clientip" => "192.168.1.252",                #IP地址匹配出来
         "ident" => "-",                           #远程登录名(由identd而来，如果支持)匹配出来
          "auth" => "-",                           #远程用户名：根据验证信息而来，如使用了Basic 认证
   "timestamp" => "30/Oct/2022:21:45:19 +0800"     #时间戳匹配出来
        "verb" => "GET",                           #请求方法匹配出来
           "url" => "/favicon.ico",                #url路径匹配出来
         "proto" => "HTTP",                        #访问协议匹配出来
           "ver" => "1.1",                         #版本匹配出来
            "rc" => "200",                         #页面返回状态码匹配出来
          "bytes" => "1",                          #页面大小匹配出来
           "referrer" => "http://192.168.1.100/",  #前导页匹配出来
         "agent" => "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0"                       #客户端主机信息匹配出来

```

#### output elasticsearch插件

```shell
在elaticsearch模块的帮助文档中查找，output 区域的配置https://www.elastic.co/guide/en/logstash/current/index.html

会发现没有必须的配置，定义一个index索引名称和host指定elaticsearch服务器的地址即可，配置多台，可以实现系统的负载均衡和高可用

[root@logstash ~]# vim /etc/logstash/conf.d/my.conf
input { 
  file {
    path => ["/tmp/c.log"]
    type => "weblog"
    start_position => "beginning"
    sincedb_path => "/var/lib/logstash/sincedb"		#位置还改为记录下次读取日志位置文件
  }
}

filter{
  grok {
    match => { "message" => "%{HTTPD_COMBINEDLOG}" }
    remove_field => ["message"]
  }
}

output{ 
  stdout{ codec => "rubydebug" }
  elasticsearch {
    hosts => ["es-0004:9200", "es-0005:9200"]
    index => "weblog-%{+YYYY.MM.dd}"
  }
}
[root@logstash ~]# /usr/share/logstash/bin/logstash

web主机获取apache日志记录
[root@web-0001 ~]# cat /var/log/httpd/access_log

[root@logstash ~]# vim /tmp/c.log		#把cat查看的最后一行数据复制到c.log中
100.125.24.26 - - [08/Jun/2023:03:35:29 +0800] "GET /es-head/ HTTP/1.1" 401 381 "-" "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36"
```

浏览器打开 head 插件，通过 web 页面浏览验证

![image-20230608220919456](cloud_01.assets/image-20230608220919456.png)

# WEB日志分析实战

## beats配置

```shell
从web服务器收集日志
  - [x] 由于 logstash 依赖 JAVA 环境，而且占用资源非常大，因此在每一台web服务器上部署 logstash 非常不合适
  - [x] 使用更轻量的 filebeat 替代
  - [x] filebeat  非常轻量，没有依赖
  - [x] filebeat  可以通过网络给 logstash 发送数据
  
beats插件使用
[root@logstash ~]# vim /etc/logstash/conf.d/my.conf
input { 
  beats {
    port => 5044
  }
} 

filter{
  grok {
    match => { "message" => "%{HTTPD_COMBINEDLOG}" }
    remove_field => ["message"]
  }
}

output{ 
  stdout{ codec => "rubydebug" }
  elasticsearch {
    hosts => ["es-0004:9200", "es-0005:9200"]
    index => "apachelog-%{+YYYY.MM.dd}"		#更改索引名字
  }
}
[root@logstash ~]# /usr/share/logstash/bin/logstash
```

## filebeat安装配置

```shell
[root@ecs-proxy ~]# ssh 192.168.1.11
[root@web-0001 ~]# yum -y install filebeat
[root@web-0001 ~]# vim /etc/filebeat/filebeat.yml
24:  enabled: true # 启用收集模块
28:  - /var/log/httpd/access_log  #定义日志路径
45   fields:					#自定义日志字段，方便用户区分日志来源
46     logtype: apache			 #定义名称
149 #output.elasticsearch:			 #加上#号
151   #hosts: ["localhost:9200"]	  #加上#号
162 output.logstash: 				 #输出到logstash
164   hosts: ["192.168.1.27:5044"]      #logstash地址
180 #processors:				     #收集系统相关信息，可以注释掉
181   #- add_host_metadata: ~		  #收集系统相关信息，可以注释掉
182   #- add_cloud_metadata: ~		  #收集系统相关信息，可以注释掉
[root@web-0001 ~]# systemctl enable --now filebeat 

使用命令行访问web-0001，内网访问
[root@web-0001 ~]# curl 192.168.1.11/info.php
```

可以看到logstash启动页面有数据，已经解析成功

![image-20230608224725484](cloud_01.assets/image-20230608224725484.png)

elasticsearch集群已经有索引数据出现

![image-20230608224751025](cloud_01.assets/image-20230608224751025.png)

### logstash额外配置

```shell
扩展：目前虽然已经可以收集到数据，但是如果logstash需要收集不同服务的日志，可以做匹配，如
	apache的日志进入logstash --》 apache的过滤规则
	nginx 的日志进入logstash --》 nginx 的过滤规则
	数据库的日志进入logstash --》 数据库的过滤规则
[root@logstash ~]# cat /etc/logstash/conf.d/my.conf
input { 
  beats {
    port => 5044
  }
}

filter{
  if [fields][logtype] == "apache" {
  grok {
    match => { "message" => "%{HTTPD_COMBINEDLOG}" }
    remove_field => ["message"]
  }}
}

output{ 
  stdout{ codec => "rubydebug" }
  if [fields][logtype] == "apache" {
  elasticsearch {
    hosts => ["es-0004:9200", "es-0005:9200"]
    index => "apachelog-%{+YYYY.MM.dd}"
  }}
}
[root@logstash ~]# /usr/share/logstash/bin/logstash
```

使用kibana进行数据可视化展示

![image-20230608230432354](cloud_01.assets/image-20230608230432354.png)

![image-20230608230456102](cloud_01.assets/image-20230608230456102.png)

![image-20230608230852913](cloud_01.assets/image-20230608230852913.png)

![image-20230608230911178](cloud_01.assets/image-20230608230911178.png)

![image-20230608230933928](cloud_01.assets/image-20230608230933928.png)

![image-20230608230948154](cloud_01.assets/image-20230608230948154.png)

![image-20230608231023247](cloud_01.assets/image-20230608231023247.png)

![image-20230608231037841](cloud_01.assets/image-20230608231037841.png)

![image-20230608231055521](cloud_01.assets/image-20230608231055521.png)

![image-20230608231115432](cloud_01.assets/image-20230608231115432.png)

![image-20230608231125790](cloud_01.assets/image-20230608231125790.png)

![image-20230608231137407](cloud_01.assets/image-20230608231137407.png)

![image-20230608231151309](cloud_01.assets/image-20230608231151309.png)



~~~shell
测试：
方法一，直接在内网中访问：
[root@logstash ~]# for i in {1..10}
> do
> curl 192.168.1.11/info.php
> done

方法二：把web-0001主机发布到互联网上，可以让其他人访问测试，查看结果
~~~

kibana页面设置刷新时间

![image-20230608231333799](cloud_01.assets/image-20230608231333799.png)

可以看到已经自动刷新

![image-20230608231525195](cloud_01.assets/image-20230608231525195.png)



# 补充

~~~shell
apache日志格式含义：
100.125.24.92 - admin [08/Jun/2023:23:36:04 +0800] "GET /es/_nodes/stats HTTP/1.1" 200 6418 "http://122.9.71.198/es-head/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"


100.125.24.92					   #IP地址
- 								  #远程登录名(由identd而来，如果支持)匹配出来
admin 							  #远程用户名：根据验证信息而来，如使用了Basic 认证
[08/Jun/2023:23:36:04 +0800] 		#时间戳
"GET 							  #请求方法
/es/_nodes/stats 				   #url路径
HTTP							  #访问协议
1.1" 							  #版本
200 							  #页面返回状态码
6418 							  #页面大小
"http://122.9.71.198/es-head/" 		#前导页
"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"	                   #客户端主机信息

注：
Identd是一个服务程序，它在服务器上运行并响应Ident协议的请求。当客户端连接到服务器时，服务器会建立一个Ident请求到客户端所在的主机，并在Identd程序上查询此请求的用户标识信息

Ident是一种协议，它用于获取用户标识信息；而Identd是一个实现Ident协议的服务程序，它用于响应Ident查询请求




使用正则匹配日志数据
####grok插件使用模板，grok的编写采用的是正则表达式
#\d 匹配0-9的数字；\S 匹配非空字符; ? 匹配前一个字符1次；. 代表任意单个字符；
#() 限定字符；+ 匹配前一个字符一次以上；\ 为转义符号，取消特殊含义
[root@logstash ~]# vim /etc/logstash/logstash.conf 
............
filter{
      grok {
        match => { "message" => "(?<client_ip>([12]?\d?\d\.){3}[12]?\d?\d) (?<rhost>\S+) (?<vber>\S+) \[(?<time>.+)\] \"(?<method>[A-Z]+) (?<url>\S+) (?<proto>[A-Z]+)\/(?<ver>[0-9.]+)\" (?<rc>\d+) (?<size>\d+) \"(?<ref>\S+)\" \"(?<agent>[^\"]+)\""}
      }
}
启动logstash服务，查看匹配结果
[root@logstash ~]# /opt/logstash/bin/logstash -f /etc/logstash/logstash.conf 
     "client_ip" => "192.168.8.1"        		  #IP地址匹配出来
         "rhost" => "-",                           #远程登录名(由identd而来，如果支持)匹配出来
          "vber" => "-",                           #远程用户名：根据验证信息而来，如使用了Basic 认证
          "time" => "30/Oct/2022:21:45:19 +0800"   #时间戳匹配出来
        "method" => "GET",                         #请求方法匹配出来
           "url" => "/favicon.ico",                #url路径匹配出来
         "proto" => "HTTP",                        #访问协议匹配出来
           "ver" => "1.1",                         #版本匹配出来
            "rc" => "200",                         #页面返回状态码匹配出来
          "size" => "1",                           #页面大小匹配出来
           "ref" => "http://192.168.8.48/",        #前导页匹配出来
         "agent" => "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0"                       #客户端主机信息匹配出来
}
~~~

